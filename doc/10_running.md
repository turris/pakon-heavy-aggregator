% Running the aggregator

This document describes how to start the aggregator.

While aimed at the turris routers, the aggregator can be run (and is usually
experimented with and tested) on normal desktop computer (with modern-enough
linux).

# Dependencies

The aggregator is just one part of the system. It only gathers and processes
data and allows querying it. The data need to be provided by a backend, which
is currently the [pakon Guts](https://gitlab.labs.nic.cz/turris/pakon-guts).

After starting that, make sure the guts' socket is accessible by the current
user (as guts needs to run as root, but aggregator is happy with an ordinary
user).

## Note about Guts

We plan on replacing that one in some not far away future. But it is what we
use now.

# Running

Then run the aggregator and let it process data (you need to have Rust
installed):

```
cargo run -- -E debug -g ../guts/socket -d socket
```

This will compile and run the aggregator. If you obtained the aggregator as a
compiled binary, you can replace `cargo run --` with the binary's invocation.

The parameters are:

* `-E level`: The log level on stderr.
* `-S level`: The log level for syslog (not used in the example).
* `-g socket`: A path to the guts daemon socket.
  Aggregator connects there and processes data coming from there.
* `-d socket`: A path to the downstream socket ‒ aggregator creates this one
  and listens on it for incoming client connections. This is where the queries
  are sent to. Beware that the socket must not exist when the aggregator starts.

## Note about frontend connections

Local frontends can connect to the socket directly. However, if it is desired
to connect from a javascript in a browser, a bridge to publish the socket must
be used. We have such a
[bridge here](https://gitlab.labs.nic.cz/turris/pakon-ws).
