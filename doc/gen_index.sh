#!/bin/sh

# Generates an index page for the documentation. It has some hardcoded links
# and then a bunch of generated ones from files in doc/

echo '<html><head><meta charset="utf-8"><title>Pakon aggregator</title></head>'
echo '<body><h1>Pakon aggregator</h1><h2>Documentation</h2>'
echo '<ul><li><a href="doc/libaggregator/index.html">API documentation</a>'
for DOC in $(ls doc/*.md | sort) ; do
	TITLE=$(sed -ne 's/^% *//p' "$DOC")
	PTH=$(echo "$DOC" | sed -s 's/^.*doc\///;s/md$/html/')
	echo "<li><a href='doc/pakon-aggregator/$PTH'>$TITLE</a>"
done
echo '</ul>'
echo '<h2>Downloads</h2><ul>'
for arch in x86-64 omnia turris ; do
	echo "<li><a href='pakon-aggregator.$arch'>pakon-aggregator.$arch</a>"
done
echo '</ul>'
echo '<h2>Coverage</h2><p>We have a test coverage <a href='kcov/'>here</a> (does not seem to work in Firefox ‒ no idea why)'.
echo '</html>'
