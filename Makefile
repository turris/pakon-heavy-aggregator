# A makefile for convenience, simply calling cargo with the right parameters.

.PHONY: all release check test clean doc ci-build ci-build-cross ci-test ci-all clippy coverage

CRATES:=$(wildcard lib*) $(wildcard dsrc*) $(wildcard int-compute*) $(wildcard test-help*)
CLIPPIES:=$(patsubst %,clippy-%,$(CRATES))
LICHKING:=$(patsubst %,lichking-%,$(CRATES))
COVERAGE:=$(patsubst %,coverage-%,$(CRATES))

.PHONY: $(CLIPPIES) $(LICHKING) $(COVERAGE)

all:
	cargo build

release:
	cargo build --release

test:
	cargo test --all
	cargo test --all i686-unknown-linux-musl

doc:
	cargo external-doc
	cargo doc --all

$(CLIPPIES): clippy-%: %
	cd $< && cargo clippy -- --deny warnings --deny clippy

clippy: $(CLIPPIES)

$(LICHKING): lichking-%: %
	cd $< && cargo lichking check

$(COVERAGE): coverage-%: %
	cd $< && cargo coverage

coverage: $(COVERAGE)
	kcov --merge `pwd`/target/kcov `pwd`/target/kcov-*

check: all test doc $(CLIPPIES)
	#cargo fmt

clean:
	cargo clean
	rm -rf $(patsubst %,%/target/,$(CRATES))

# Allow running CI-like checks and builds locally from make
# It expects your local environment to default to nightly

ci-all:
	$(MAKE) ci-build-cross
	$(MAKE) ci-build
	$(MAKE) ci-test

ci-build: doc
	rustup run stable cargo build --all --release

ci-build-cross:
	rustup run stable cargo build --all --release --target armv7-unknown-linux-musleabihf
	# Unfortunately, as xargo compiles the std crate from sources and that needs
	# unlocking the unstable features (but the unstable features used correspond
	# to the unstable features supported by the compiler, because they come
	# together). The easiest way is to use nightly. If that's a problem in
	# production, we can fix the nightly's version to some release version or
	# compile the compiler ourselves with the unstables unlocked.
	cp -r `rustup run $$NIGHTLY rustc --print sysroot`/lib/rustlib/src/rust/src/ rust-src
	sed -i -e 's/\[deny/[allow/' rust-src/libstd/build.rs
	XARGO_RUST_SRC=`pwd`/rust-src xargo build --all --release --target powerpc-unknown-linux-gnu

ci-test: $(CLIPPIES) $(LICHKING)
	rustup run stable cargo test --all
	rustup run stable cargo test --all --target i686-unknown-linux-musl
	if grep -R -q XXX $(wildcard $(patsubst %,%/*.rs,$(CRATES))) ; then echo "Forgotten XXX (you meant to implement it, right?)" ; false ; fi
	# Check licenses
	# We are not synced to the newest rustfmt currently, temporarily disabled,
	# will be solved in a separate branch
	# cargo fmt
