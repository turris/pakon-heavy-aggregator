// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! Abstract data source
//!
//! The data source is something that provides information (like from the kernel, external
//! programs, etc) the aggregator then connects together and keeps.
//!
//! This crate contains the abstract interface, other crates contain the concrete implementations
//! and can be turned on or off at compile time by features.

#![deny(missing_docs)]

extern crate slog;
extern crate tokio_core;

extern crate libflow;
extern crate libutils;

use slog::Logger;
use tokio_core::reactor::Handle;

use libflow::update::{Cork, UpdateSender};
use libutils::LocalBoxFuture;

/// A factory for data source.
///
/// This trait is used to create a data source of one kind.
pub trait Factory {
    /// Creates a new instance of the data source.
    ///
    /// # Params
    ///
    /// * `logger`: Used for general logging output. It is expected the factory creates a child
    ///   logger for the new source and names it appropriately (eg. sets the `context` variable).
    /// * `handle`: A handle to the core, if the data source needs to spawn some futures.
    /// * `channel`: A channel the data source shall push live updates into.
    ///
    /// # Result
    ///
    /// * The new data source.
    /// * A future signaling termination or error of the data source. The data source should
    ///   generally not terminate, as that causes termination of the whole aggregator. If there's
    ///   no meaningful way to provide such future, `futures::future::empty` might be appropriate.
    fn create(self, logger: &Logger, handle: Handle, channel: UpdateSender)
        -> (Box<Dsrc>, LocalBoxFuture<(), ()>);
}

/// The asbract data source.
///
/// The data source is plugged into the reactor and provides information about flows.
///
/// It is expected to emit live updates using the channel passed to it during its creation. It is
/// also called repeatedly just before a slice is closed, to provide timed information.
pub trait Dsrc {
    /// This gets called by the reactor before every slice closing.
    ///
    /// The data source shall push all updates that need to happen before the slice closing into
    /// its channel (passed to it during creation through
    /// [`Factory::create`](trait.Factory.html#method.create)). Then it should end the batch with
    /// the provided cork.
    ///
    /// If there are no updates that need to happen as a result of a tick (eg. the data source has
    /// no notion of repeated data, everything is based on some external events), then the cork can
    /// be simply dropped right away.
    ///
    /// # Params
    ///
    /// * `cork`: The cork to use for terminating the update batch.
    fn tick(&mut self, cork: Cork);
}
