// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! The handler of a downstream connections.
//!
//! This handles the downstream connections from our clients and manages their answers.

use std::cell::RefCell;
use std::collections::HashMap;
use std::io::Error as IoError;
use std::rc::Rc;

use futures::{Future, IntoFuture};
use futures::future::Either;
use serde_json::Value;
use slog::Logger;
use tokio_core::reactor::Handle;
use tokio_io::{AsyncRead, AsyncWrite};
use tokio_jsonrpc::{Endpoint, LineCodec, RpcError, Server, ServerCtl};

use keeper::Keeper;
use libquery::query::{Query, Response as QueryResponse};
use libutils::LocalBoxFuture;
use libutils::tunable::MAX_CLIENTS;
use wakeup::{Manager as WakeupManager, Wakeup};

/// An enum representing the possible responses.
///
/// This is just to allow returning different things from different methods and have it
/// auto-converted to the JSON representation.
#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
#[serde(untagged)]
enum Response {
    /// When sending the response to a query.
    QueryResponse(QueryResponse),
    /// When we want to answer in affirmative, but have no data.
    EmptyResponse,
}

/// The major part of API version we report
const API_VERSION_MAJOR: usize = 0;
/// The minor part of API version we report
const API_VERSION_MINOR: usize = 2;
/// The list of features we report
const API_FEATURES: &[&str] = &[];

/// The parameters of the `repeated` RPC.
///
/// A helper structure to get automatic parsing of parameters.
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
struct RepeatedParams {
    /// The ID of the query instance.
    id: String,
    /// The query itself.
    ///
    /// If `None`, the RPC tries to disable an existing query.
    query: Option<Query>,
}

/// The internal part of a downstream server.
///
/// The real downstream server is just an `Rc` wrapper around this, because we need to link to it
/// from multiple places.
struct DownstreamServerInternal {
    /// The logger used for the downstream server.
    logger: Logger,
    /// The keeper of the history.
    keeper: Keeper,
    /// The tokio handle
    handle: Handle,
    /// A server control.
    ///
    /// This is used whenever we want to uniliterally send notification to the frontend (because
    /// the client has registered a repeated query). We can't keep the client endpoint directly,
    /// as that would create a reference cycle and prevent it from being dropped. So we create a
    /// client on-demand.
    ///
    /// Note that this is `Some(ctl)` for the whole lifetime, it is an `Option` only because we
    /// don't know it before initialization.
    ctl: Option<ServerCtl>,
    /// Registered repeated queries (id → query).
    queries: HashMap<String, Query>,
}

impl DownstreamServerInternal {
    /// Creates a new internal server.
    fn new(logger: Logger, keeper: Keeper, handle: Handle) -> Self {
        Self {
            logger,
            keeper,
            handle,
            ctl: None,
            queries: HashMap::new(),
        }
    }
    /// Runs a query for this connection.
    ///
    /// It only runs the query (using the keeper and logger inside), it doesn't send it in any way.
    fn query(&self, query: &Query) -> LocalBoxFuture<Response, RpcError> {
        let logger = self.logger.new(o!("context" => "query"));
        let future_response = self.keeper.query(&logger, query).map_err(|e| {
                // Currently, it is not clear how or if at all the query can fail. But convert
                // the erorr anyway.
                RpcError::server_error(Some(format!("Query failed: {}", e)))
            })
            .map(Response::QueryResponse);
        Box::new(future_response) // We want impl Trait :-(
    }
}

impl Wakeup for RefCell<DownstreamServerInternal> {
    // We want to send results of repeated queries to the client whenever there are new data in the
    // keeper.
    fn wakeup(&self) {
        // TODO: Make the parallel processing bounded somehow?
        let inner = self.borrow();
        for (id, query) in &inner.queries {
            let cli = match inner.ctl.as_ref().expect("Missing server ctl").client() {
                Ok(cli) => cli,
                // It may have happened the connection died in the meantime. It'll get
                // garbage-collected soon, but avoid crashing on a race condition.
                Err(_) => return,
            };
            let logger1 = inner.logger.clone();
            let logger2 = inner.logger.clone();
            let id = id.to_owned();
            let sent = inner.query(query)
                .map_err(move |_| error!(logger1, "Failed to provide repeated query"))
                .and_then(move |response| {
                    let msg = json!({
                        "id": id,
                        "result": response,
                    });
                    cli.notify("repeated-result".to_owned(), Some(msg))
                        .map_err(move |e| error!(logger2, "Failed to send: {}", e))
                        .map(|_| ())
                });
            inner.handle.spawn(sent);
        }
    }
}

/// The implementation of the internal `Server` for the downstream connection.
#[derive(Clone)] // Note that this structure is lightweight to clone, as it's just an Rc
struct DownstreamServer(Rc<RefCell<DownstreamServerInternal>>);

impl DownstreamServer {
    /// Call the query RPC on the keeper and return the future of the response.
    fn do_query(&self, params: &Option<Value>) -> LocalBoxFuture<Response, RpcError> {
        debug!(self.0.borrow().logger, "Running a query");
        let me = self.clone();
        let response = jsonrpc_params!(params, wrap "query" => Query)
            .into_future()
            .and_then(move |(query,)| {
                let inner = me.0.borrow();
                inner.query(&query)
            });
        Box::new(response)
    }
    /// Registers a query to be called every now and then and reported to the user
    fn register_query(&self, id: String, query: Query) {
        self.0.borrow_mut().queries.insert(id, query);
    }
    /// Deregisters a query if it exists.
    fn deregister_query(&self, id: String) {
        self.0.borrow_mut().queries.remove(&id);
    }
    /// Call the repeated RPC, and keep calling repeatedly.
    ///
    /// Or deregister it, if no query passed.
    fn do_repeated(&self, params: &Option<Value>) -> LocalBoxFuture<Response, RpcError> {
        let me = self.clone();
        let logger = self.0
            .borrow()
            .logger
            .clone();
        let response = jsonrpc_params!(params, wrap "params" => RepeatedParams)
            .into_future()
            .and_then(move |(RepeatedParams { id, query },)| {
                match query {
                    Some(query) => {
                        debug!(logger, "Running the 'repeated' RPC {}", id);
                        let f = {
                            // Fights with the borrow checker here :-(
                            let inner = me.0.borrow();
                            inner.query(&query)
                        };
                        // Store the query for future, but only if it succeeded
                        let registered = f.map(move |response| {
                            debug!(logger, "Registering query {}", id);
                            me.register_query(id, query);
                            response
                        });
                        Either::A(registered)
                    },
                    None => {
                        debug!(logger, "Disabling query {}", id);
                        me.deregister_query(id);
                        Either::B(Ok(Response::EmptyResponse).into_future())
                    }
                }
            });
        Box::new(response)
    }
}

impl Server for DownstreamServer {
    type Success = Response;
    type RpcCallResult = LocalBoxFuture<Response, RpcError>;
    type NotificationResult = Result<(), ()>;
    fn rpc(&self, _ctl: &ServerCtl, method: &str, params: &Option<Value>)
           -> Option<Self::RpcCallResult> {
        match method {
            "query" => Some(self.do_query(params)),
            "repeated" => Some(self.do_repeated(params)),
            _ => None,
        }
    }
    fn initialized(&self, ctl: &ServerCtl) {
        let mut inner = self.0.borrow_mut();
        // Store the server ctl, so we can access it from somewhere else
        inner.ctl = Some(ctl.clone());
        // Greet the client
        debug!(inner.logger, "Greeting a new client with version notification";
               "major" => API_VERSION_MAJOR,
               "minor" => API_VERSION_MINOR);
        // Introduce the server by sending the version notification
        let client = ctl.client().unwrap();
        let msg = json!({
            "major": API_VERSION_MAJOR,
            "minor": API_VERSION_MINOR,
            "features": API_FEATURES
        });
        let notified = client.notify("version".to_owned(), Some(msg));
        inner.handle.spawn(notified.map(|_| ()).map_err(|_| ()));
    }
}

/// Handles one connected client.
///
/// This creates a server object for the given client and runs it in the given handle. The returned
/// future resolves one the connection terminates.
///
/// # Parameters
///
/// * `logger`: Log relevant messages to this logger.
/// * `handle`: The handle to tokio's `Core` where the server should run.
/// * `connection`: The raw connection from the client.
/// * `keeper`: The flow history keeper to query.
/// * `manager`: A wakeup manager the client registers itself into, to be able to provide repeated
///   queries every time new data is available.
pub fn handle<C>(logger: Logger, handle: &Handle, connection: C, keeper: Keeper,
                 manager: &WakeupManager)
                 -> LocalBoxFuture<(), IoError>
    where
        C: AsyncRead + AsyncWrite + Send + 'static
{
    debug!(logger, "New client");
    let server_logger = logger.new(o!("context" => "downsrv"));
    let inner = DownstreamServerInternal::new(server_logger, keeper, handle.clone());
    let inner_rc = Rc::new(RefCell::new(inner));
    let inner_weak = Rc::downgrade(&inner_rc);
    let server = DownstreamServer(inner_rc);
    // TODO: Some timeouts on inactivity
    let (_client, terminated) = Endpoint::new(connection.framed(LineCodec::new()), server)
        .logger(logger)
        .parallel(MAX_CLIENTS)
        .start(handle);
    // By now it has been initialized, so the missing ctl is added. We can now plug it
    // into the list of repeated query containers.
    manager.register(inner_weak);
    terminated
}

#[cfg(test)]
mod tests {
    use std::io::{BufRead, BufReader, Cursor};
    use std::str;
    use std::time::Duration;

    use tokio_core::reactor::{Core, Timeout};
    use tokio_io::io;
    use tokio_uds::UnixStream;

    use super::*;
    use libutils;
    use test_help;

    /// A test for handling a connection.
    ///
    /// We send a query and repeated RPC. We expect to get the version notification and successful
    /// results to both. We further expect a repeated query result notification when we trigger the
    /// wakeup.
    #[test]
    fn handle_conn() {
        let mut core = Core::new().unwrap();
        let core_handle = core.handle();
        let logger = libutils::test_logger();
        let (cli, serv) = UnixStream::pair(&core_handle).unwrap();
        let keeper = Keeper::dummy();
        let manager = WakeupManager::new();
        // We assume the unix-domain socket buffers are large enough. We first send all the
        // requests and then start receiving. If the buffers were too small, this could in theory
        // deadlock.
        let terminated = handle(logger, &core_handle, serv, keeper, &manager);
        let send1 = test_help::EMPTY_QUERY.as_bytes();
        let send2 = test_help::EMPTY_REPEATED.as_bytes();
        // Exchange data with the server
        let exchange = io::write_all(cli, send1)
            .and_then(|(cli, _)| io::write_all(cli, send2))
            .and_then(|(cli, _)| io::flush(cli))
            // We should get 3 lines (version notification, two responses)
            .and_then(|cli| io::read_until(BufReader::new(cli), b'\n', Vec::new()))
            .and_then(|(cli, partial)| io::read_until(cli, b'\n', partial))
            .and_then(|(cli, partial)| io::read_until(cli, b'\n', partial));
        let timeout = Timeout::new(Duration::from_secs(5), &core_handle).unwrap();
        let (cli, response) = match core.run(exchange.select2(timeout)) {
            Ok(Either::A(((cli, response), _timeout))) => (cli, response),
            _ => panic!("Didn't exchange"),
        };
        // Check the response is successful (this is not a very thorough check, but we are OK with
        // any successful response here. The actual content of the result is checked in other
        // tests. Do count how many responses we have of each type.
        let mut vers = 0;
        let mut query = 0;
        let mut repeated = 0;
        for line in Cursor::new(response).lines() {
            let line = line.unwrap();
            println!("{}", line);
            if line.contains(r#""version""#) {
                vers += 1;
            } else if line.contains(r#""id":1"#) && line.contains(r#""result":"#) {
                query += 1;
            } else if line.contains(r#""id":2"#) && line.contains(r#""result":"#) {
                repeated += 1;
            }
        }
        assert_eq!(1, vers);
        assert_eq!(1, query);
        assert_eq!(1, repeated);
        // Let it produce repeated query notification
        manager.wakeup();
        let exchange = io::read_until(cli, b'\n', Vec::new());
        let timeout = Timeout::new(Duration::from_secs(5), &core_handle).unwrap();
        let (cli, response) = match core.run(exchange.select2(timeout)) {
            Ok(Either::A(((cli, response), _timeout))) => (cli, response),
            _ => panic!("Didn't exchange"),
        };
        let response = str::from_utf8(&response).unwrap();
        assert!(response.contains(r#""method":"repeated-result""#));
        assert!(response.contains(r#""id":"repeated""#));
        // This is a bit of a smoke test ‒ we kill the connection and try to poke the server part
        // again, to see what happens and that nothing crashes.
        drop(cli);
        manager.wakeup();
        // Let it have some time to try doing something in the background.
        let timeout = Timeout::new(Duration::from_secs(5), &core_handle).unwrap();
        match core.run(terminated.select2(timeout)) {
            // It wen't well
            Ok(Either::A(_)) => (),
            // Dead connection may rightfully lead to IO errors, so this is OK too.
            Err(Either::A(_)) => (),
            // Either a timeout or error
            _ => panic!("Didn't terminate in time"),
        }
    }
}
