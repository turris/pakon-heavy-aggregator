// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! The library of the aggregator.
//!
//! Most of the aggregator is implemented in the library instead of the binary. The reason is
//! mostly testing and documentation support, the library is not expected to be reused by some
//! other programs.

#![deny(missing_docs)]

extern crate dsrc;
extern crate futures;
extern crate int_compute;
extern crate itertools;
extern crate libc;
extern crate libdata;
extern crate libflow;
extern crate libgather;
extern crate libquery;
extern crate libutils;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate slog;
extern crate tk_listen;
extern crate tokio_core;
extern crate tokio_io;
#[macro_use]
extern crate tokio_jsonrpc;
extern crate tokio_signal;
extern crate tokio_uds;
#[cfg(test)]
extern crate test_help;
#[cfg(test)]
extern crate test_help_flow;

mod downstream;
mod keeper;
mod wakeup;

pub mod reactor;
