// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! The [`Keeper`](struct.Keeper.html) keeps the history and allows querying it.
//!
//! The module contains the `Keeper` itself as well as many support data types and routines.

use std::cell::RefCell;
use std::collections::{BTreeMap, VecDeque};
use std::io::Error as IoError;
use std::iter;
use std::rc::Rc;
use std::time::SystemTime;

use futures::IntoFuture;
use itertools::Itertools;
use slog::Logger;

use libdata::column::{Headers, Ident, Value};
use libdata::time::Timeline;
use libdata::stats::Stats;
use libflow::slice::{Flow, Time as TimeSlice};
use libquery::{Container, InTimeInterval, PreFilterResult, ValueSrc};
use libquery::query::{Query, Response, ResponseBucket};
use libutils::LocalBoxFuture;
use libutils::tunable::SLICE_MAX_CACHE;

/// Just bunch of flows aggregated by some criteria.
///
/// This is an intermediate product of the query processing.
type Aggregated<'a> = BTreeMap<Vec<Value>, Vec<&'a Flow>>;

/// Takes bunch of flow slices and aggregates them together according to the given query.
fn slices_aggregate<'a, FlowIter>(keys: &[Ident], interesting_flows: FlowIter) -> Aggregated<'a>
    where
        FlowIter: Iterator<Item = &'a Flow>
{
    let mut aggregated = BTreeMap::new();
    for flow_slice in interesting_flows {
        // Find the partial result this flow slice falls into.
        let aggr_key = keys
            .iter()
            // Skip non-existing values. It's OK, we can't really produce two same vectors by
            // missing the None's unless they miss on the same places.
            .filter_map(|criterion| flow_slice.value(criterion))
            .collect::<Vec<_>>();
        aggregated.entry(aggr_key).or_insert_with(Vec::new).push(flow_slice);
    }
    aggregated
}

/// Takes a bunch of flow slices and turns them into a single response bucket.
///
/// The statistics will be split according to the provided timeline.
///
/// # Parameters
///
/// * `timeline`: A timeline used to split the statistics by.
/// * `columns`: Includes the requested columns only in the response.
/// * `slices`: The flow slices to put into the bucket.
fn slices_to_bucket<'s, S>(timeline: &Timeline, columns: &[Ident], slices: S) -> ResponseBucket
    where
        S: Iterator<Item = &'s Flow>,
{
    // Regroup the columns together and flatten the sets
    let mut headers = Headers::new();
    let mut stats = iter::repeat(Stats::default())
        .take(timeline.interval_count())
        .collect::<Vec<_>>();
    for flow_slice in slices {
        // Insert the stats into the correct interval
        let slice_stat = flow_slice.stats();
        let (int_start, int_end) = flow_slice.interval();
        let interval = timeline.place(int_start, int_end);
        // This is guaranteed to fit, since the timeline knows how many intervals it has.
        stats[interval].combine_overlay(slice_stat);
        // Add all the columns
        for column in columns {
            headers.insert_opt(*column, flow_slice.value(column));
        }
    }
    ResponseBucket {
        headers,
        stats,
    }
}

/// An internal part of the keeper, shared through reference counting.
///
/// The real [`Keeper`](struct.Keeper.html) is just an `Rc` wrapper around this.
#[derive(Debug)]
struct KeeperInternal {
    /// A logger used to log interesting events.
    ///
    /// This one is used for general upkeep of the internal data, but not for queries, since
    /// queries are client-related and therefore carry information about the connection.
    logger: Logger,
    /// The buffer of time slices kept in memory.
    ///
    /// # TODO
    ///
    /// This representation is likely to change in the future, once we have some kind of disk-based
    /// stogare. For now, this is simply a queue of the last few time slices we can query.
    slices: VecDeque<TimeSlice>,
}

/// A history keeper.
///
/// This holds and manages history of the flows. It keeps the in-memory part of history as well as
/// acts as a proxy for the on-disk data.
///
/// This structure is expected to be direcly shared by multiple owners ‒ it contains an `Rc`
/// internally. Therefore, you can `clone()` it cheaply and all the clones refer to the same
/// keeper.
#[derive(Clone, Debug)]
pub struct Keeper(Rc<RefCell<KeeperInternal>>);

impl Keeper {
    /// Creates a new `Keeper`.
    ///
    /// # Parameters:
    ///
    /// * `logger`: Will be used to log messages from the keeper.
    pub fn new(logger: Logger) -> Self {
        let internal = KeeperInternal {
            logger,
            slices: VecDeque::new(),
        };
        Keeper(Rc::new(RefCell::new(internal)))
    }

    /// Runs a query on the available data.
    ///
    /// # TODO: Better documentation, once it does something.
    pub fn query(&self, logger: &Logger, query: &Query) -> LocalBoxFuture<Response, IoError> {
        let internal = self.0.borrow();
        let absolute_start = query.start.clone().map(Into::<SystemTime>::into);
        let absolute_end = query.end.clone().map(Into::<SystemTime>::into);
        debug!(logger, "Running query {:?} on in-memory data", query);
        let containers = (&internal.slices)
            .into_iter()
            .filter(|time_slice| time_slice.in_time_interval(absolute_start, absolute_end))
            // TODO: We may want to be more clever here once we have the on-disk data, since we may
            // have multiple containers with the same interval and we want just one of them. We
            // also want to refuse the whole query in case we have the data but not the right
            // columns. But the current only implementation always returns `MayUse`, so this is is
            // enough for now.
            .filter(|time_slice| match time_slice.pre_filter(query) {
                PreFilterResult::MayUse => true,
                PreFilterResult::NoData |
                PreFilterResult::MissingColumns => unreachable!("in-memory is always MayUse"),
            })
            .collect::<Vec<_>>();
        let mut timeline = Timeline::new();
        trace!(logger, "Computing the timeline from {} containers", containers.len());
        for cont in &containers {
            let mut cont_timeline = cont.timeline();
            cont_timeline.clip(absolute_start, absolute_end);
            timeline.combine(cont_timeline);
        }
        trace!(logger, "Computed timeline {:?}", timeline);
        let interesting_flows = containers
            .into_iter()
            .flat_map(|time_slice| time_slice.into_iter())
            // In the interval
            .filter(|flow_slice| flow_slice.in_time_interval(absolute_start, absolute_end))
            // Passes all the requested criteria
            .filter(|flow| {
                for (ident, values) in &query.filter {
                    let value = flow.value(ident);
                    if !values.contains(&value) {
                        return false;
                    }
                }
                true
            });

        trace!(logger, "Aggregating flows");
        let aggregated = slices_aggregate(&query.aggregate, interesting_flows);
        // We want the result to contain both the columns we aggregate by and the columns the user
        // asked for explicitly. Make sure we have it only once even if it is specified in both.
        let interesting_columns = query.aggregate.iter()
            .cloned()
            .chain(query.columns.iter().cloned())
            .unique()
            .collect::<Vec<_>>();
        trace!(logger,
               "Producing {} response buckets with {} columns",
               aggregated.len(),
               interesting_columns.len());
        let mut buckets = aggregated
            .into_iter()
            .map(|(_k, v)| slices_to_bucket(&timeline, &interesting_columns, v.into_iter()))
            .collect::<Vec<_>>();
        if !query.details {
            // The details are not asked for. So, flatten the timeline and the statistics on all
            // the buckets.
            trace!(logger, "Flattening the timeline");
            timeline = Timeline::new();
            for bucket in &mut buckets {
                bucket.flatten_stats();
            }
        }
        let response = Response {
            buckets,
            timeline,
        };
        Box::new(Ok(response).into_future())
    }

    /// Appends another time slice to the keeper.
    ///
    /// # TODO
    ///
    /// This will also store them to disk and manage the aggregation of old ones.
    pub fn append(&self, slice: TimeSlice) {
        let mut internal = self.0.borrow_mut();
        debug!(internal.logger, "Appending a time slice to history");
        internal.slices.push_back(slice);
        // Drop extra elements (the oldest ones)
        while internal.slices.len() > SLICE_MAX_CACHE {
            internal.slices.pop_front();
        }
    }

    /// Creates a new `Keeper` for tests.
    ///
    /// Currently, there's no difference between `new` and `dummy`. But in the future, `dummy`
    /// won't read (or write) any data to disk.
    #[cfg(test)]
    pub fn dummy() -> Self {
        Self::new(::libutils::test_logger())
    }
}

#[cfg(test)]
mod tests {
    use std::net::IpAddr;
    use std::time::{Duration, UNIX_EPOCH};

    use futures::Future;

    use super::*;
    use libdata::column::{Ident, Local, Remote};
    use libdata::flow::{self, Bytes, Count, MacName, Speed};
    use libdata::stats::Stat;
    use libdata::time::Time;
    use libutils;
    use libutils::tunable::{SLICE_LENGTH_SECONDS, SLICE_MAX_CACHE};
    use test_help_flow::{dummy_flow, dummy_time, flow_append};

    /// Check appending to the keeper works and it drops the oldest items when it overflows.
    #[test]
    fn append() {
        let keeper = Keeper::dummy();
        keeper.append(dummy_time(1));
        let len = || keeper.0.borrow().slices.len();
        assert_eq!(len(), 1);
        // Make it overflow
        for i in 2..SLICE_MAX_CACHE + 2 {
            keeper.append(dummy_time(i as u64));
        }
        assert_eq!(len(), SLICE_MAX_CACHE);
        let inner = keeper.0.borrow();
        assert_eq!(inner.slices.front().unwrap(), &dummy_time(2));
        assert_eq!(inner.slices.back().unwrap(), &dummy_time(SLICE_MAX_CACHE as u64 + 1));
    }

    /// A helper to modify the given bucket by adding times small dummy flows.
    fn bucket_add_dummy_flow(bucket: ResponseBucket, times: usize) -> ResponseBucket {
        // The stats don't know multiplication, so simulate it by a cycle
        let mut dummy_half = Stat::default();
        for _ in 0..times {
            dummy_half.combine_overlay(&Stat {
                packets: Count(1),
                size: Bytes(120),
                max_speed: Speed(24),
                speed_duration: Duration::from_secs(SLICE_LENGTH_SECONDS),
                flows: Count(1),
                flows_started: Count(0),
                start: Some(UNIX_EPOCH),
                end: Some(UNIX_EPOCH + Duration::from_secs(SLICE_LENGTH_SECONDS)),
            });
        }
        let dummy_flow = Stats {
            dir_in: dummy_half,
            dir_out: Stat {
                flows: Count(times as u64),
                start: Some(UNIX_EPOCH),
                end: Some(UNIX_EPOCH + Duration::from_secs(SLICE_LENGTH_SECONDS)),
                ..Stat::default()
            },
        };
        let stats = bucket.stats
            .into_iter()
            .map(|mut s| {
                s.combine_overlay(&dummy_flow);
                s
            })
            .collect();
        ResponseBucket {
            stats,
            .. bucket
        }
    }

    /// A test fixture to get some basic items created and shared with multiple tests.
    ///
    /// This provides a way to share some variables across multiple tests.
    struct FlowTests {
        local_ip: IpAddr,
        remote_ip_a: IpAddr,
        remote_ip_b: IpAddr,
        flows: [Flow; 3],
        ip_columns: [Ident; 2],
        combined_bucket: ResponseBucket,
    }

    impl FlowTests {
        /// A constructor for the test fixture
        fn new() -> Self {
            let local_ip = "192.0.2.1".parse::<IpAddr>().unwrap();
            let remote_ip_a = "192.0.2.2".parse::<IpAddr>().unwrap();
            let remote_ip_b = "192.0.2.3".parse::<IpAddr>().unwrap();

            let mut headers = Headers::new();
            headers.insert(Local(local_ip));
            headers.insert(Remote(remote_ip_a));
            headers.insert(Remote(remote_ip_b));
            let combined_bucket = ResponseBucket {
                headers,
                stats: vec![Stats::default()],
            };
            Self {
                local_ip,
                remote_ip_a,
                remote_ip_b,
                flows: [
                    dummy_flow(local_ip, remote_ip_a),
                    dummy_flow(local_ip, remote_ip_b),
                    dummy_flow(local_ip, remote_ip_b),
                ],
                ip_columns: [
                    Ident::of::<Local<IpAddr>>(),
                    Ident::of::<Remote<IpAddr>>(),
                ],
                combined_bucket,
            }
        }
    }

    /// Tests aggregation of flows into buckets according to some criteria.
    ///
    /// The test uses just the IP addresses, assuming it doesn't matter as long as we know if they
    /// are the same or different.
    #[test]
    fn aggregate() {
        let data = FlowTests::new();
        // If we have no keys for aggregation, everything should end up in the same bucket
        let all_together = slices_aggregate(&[], data.flows.iter());
        assert_eq!(1, all_together.len());
        assert_eq!(3, all_together.get(&Vec::new()).unwrap().len());

        // If we aggregate by local IP addres, we also get everything together, because there's
        // only one local IP address
        let local_id = [Ident::of::<Local<IpAddr>>()];
        let local_key = vec![Value::from(Local(data.local_ip))];
        let local_aggregated = slices_aggregate(&local_id, data.flows.iter());
        assert_eq!(1, local_aggregated.len());
        assert_eq!(3, local_aggregated.get(&local_key).unwrap().len());

        // If we do it according to the remote address, we get two buckets, one with 1 flow,
        // the other with two.
        let remote_id = [Ident::of::<Remote<IpAddr>>()];
        let remote_key_a = vec![Value::from(Remote(data.remote_ip_a))];
        let remote_key_b = vec![Value::from(Remote(data.remote_ip_b))];
        let remote_aggregated = slices_aggregate(&remote_id, data.flows.iter());
        assert_eq!(2, remote_aggregated.len());
        assert_eq!(1, remote_aggregated.get(&remote_key_a).unwrap().len());
        assert_eq!(2, remote_aggregated.get(&remote_key_b).unwrap().len());

        // Combining aggregation by both works the same way (because it differs only in
        // one of the keys)
        let combined_key_a = vec![local_key[0].clone(), remote_key_a[0].clone()];
        let combined_key_b = vec![local_key[0].clone(), remote_key_b[0].clone()];
        let combined_aggregated = slices_aggregate(&data.ip_columns, data.flows.iter());
        assert_eq!(2, combined_aggregated.len());
        assert_eq!(1, combined_aggregated.get(&combined_key_a).unwrap().len());
        assert_eq!(2, combined_aggregated.get(&combined_key_b).unwrap().len());
    }

    /// Tests generation of a response bucket from bunch of flows.
    #[test]
    fn response_bucket() {
        let data = FlowTests::new();
        let timeline = Timeline::new();

        let empty_response = ResponseBucket {
            stats: vec![Stats::default()],
            .. ResponseBucket::default()
        };

        // No flows → no columns (even empty ones)
        let empty_flows = slices_to_bucket(&timeline, &data.ip_columns, [].iter());
        assert_eq!(empty_response, empty_flows);

        // No columns asked → nothing in the columns, but the 3 flows add up to the
        // stats
        let empty_columns = slices_to_bucket(&timeline, &[], data.flows.iter());
        assert_eq!(bucket_add_dummy_flow(empty_response.clone(), 3), empty_columns);

        let all_flows = slices_to_bucket(&timeline, &data.ip_columns, data.flows.iter());
        assert_eq!(bucket_add_dummy_flow(data.combined_bucket.clone(), 3), all_flows);
    }

    /// A full query processing against the keeper.
    #[test]
    fn query() {
        let data = FlowTests::new();
        let keeper = Keeper::dummy();
        let logger = libutils::test_logger();

        // Run the query against the keeper.
        let run_query = |query: &Query| -> Response {
            let result = keeper
                .query(&logger, query)
                // We can afford to just .wait() on that future, since it is a fake one for now
                // (the result is ready right away).
                .wait()
                .unwrap();
            result.assert_sane();
            result
        };

        // First, run a query against an empty keeper. It should (obviously) produce an empty
        // result.
        let simple_query = Query {
            aggregate: vec![
                Ident::of::<Local<IpAddr>>(),
                Ident::of::<Remote<MacName>>(),
            ],
            columns: vec![Ident::of::<Remote<IpAddr>>()],
            .. Query::default()
        };
        let empty_result = run_query(&simple_query);
        assert_eq!(Response::default(), empty_result);

        // Now, put some data into the keeper.
        let mut slice = dummy_time(0);
        for f in &data.flows {
            slice = flow_append(&slice, f.clone());
        }
        keeper.append(slice);

        // When we run the query against that, we should get some buckets in the output.
        let full_result = run_query(&simple_query);
        let mut expected_full = Response {
            buckets: vec![bucket_add_dummy_flow(data.combined_bucket.clone(), 3)],
            timeline: Timeline::new(),
        };
        expected_full.buckets[0].headers.insert_empty(Ident::of::<Remote<MacName>>());
        assert_eq!(expected_full, full_result);

        // If we duplicate the local IP column in the columns, nothing bad happens (we are
        // allowed to specify it in both the aggregate and columns parameters).
        let duplicit_query = Query {
            columns: vec![
                Ident::of::<Local<IpAddr>>(),
                Ident::of::<Remote<IpAddr>>(),
            ],
            .. simple_query
        };
        let full_result = run_query(&duplicit_query);
        assert_eq!(expected_full, full_result);

        // Ask for more details. The timeline should get split accordingly.
        let detailed_query = Query {
            details: true,
            .. duplicit_query.clone()
        };
        let detailed_result = run_query(&detailed_query);
        expected_full.timeline = Timeline::new();
        expected_full.timeline.split(flow::system_time_from_ms(0));
        expected_full.timeline.split(flow::system_time_from_ms(5000));
        expected_full.buckets[0].stats.insert(0, Stats::default());
        expected_full.buckets[0].stats.push(Stats::default());
        assert_eq!(expected_full, detailed_result);

        // More details, but the timeline gets clipped
        let clipped_query = Query {
            start: Some(Time::Absolute(flow::system_time_from_ms(0))),
            end: Some(Time::Absolute(flow::system_time_from_ms(4900))),
            .. detailed_query
        };
        let clipped_result = run_query(&clipped_query);
        expected_full.timeline = Timeline::new();
        expected_full.timeline.split(flow::system_time_from_ms(0));
        expected_full.timeline.split(flow::system_time_from_ms(4900));
        assert_eq!(expected_full, clipped_result);

        // When we limit the remote IP to just one of them, we get a smaller output.
        let mut filter = Headers::new();
        filter.insert(Remote(data.remote_ip_a));
        // This filter matches everything, because we have no mac name there
        filter.insert_empty(Ident::of::<Remote<MacName>>());
        let filtered_query = Query {
            filter,
            ..duplicit_query
        };
        let half_result = run_query(&filtered_query);
        let mut headers = Headers::new();
        headers.insert(Local(data.local_ip));
        headers.insert(Remote(data.remote_ip_a));
        let half_bucket = ResponseBucket {
            headers,
            stats: vec![Stats::default()],
        };
        let mut expected_half = Response {
            buckets: vec![bucket_add_dummy_flow(half_bucket, 1)],
            timeline: Timeline::new(),
        };
        expected_half.buckets[0].headers.insert_empty(Ident::of::<Remote<MacName>>());
        assert_eq!(expected_half, half_result);

        // We further limit the time interval, so nothing falls in. Therefore we get no results
        // again.
        let interval_query = Query {
            start: Some(Time::Absolute(flow::system_time_from_ms(4800))),
            ..filtered_query
        };
        let out_of_interval = run_query(&interval_query);
        assert_eq!(Response::default(), out_of_interval);

        // If we filter by no local IP address, nothing matches (everything has some IP address)
        let mut filter = Headers::new();
        filter.insert_empty(Ident::of::<Local<IpAddr>>());
        let no_ip_query = Query {
            filter: filter.clone(),
            ..Query::default()
        };
        let no_ip = run_query(&no_ip_query);
        assert_eq!(Response::default(), no_ip);

        // But when we add the other IP address, we get everything again (note we still have that
        // no-ip one there as well).
        filter.insert(Local(data.local_ip));
        let some_ips_query = Query {
            filter,
            columns: vec![
                Ident::of::<Local<IpAddr>>(),
                Ident::of::<Remote<IpAddr>>(),
            ],
            ..Query::default()
        };
        let some_ips = run_query(&some_ips_query);
        let some_ips_expected = Response {
            buckets: vec![bucket_add_dummy_flow(data.combined_bucket.clone(), 3)],
            timeline: Timeline::new(),
        };
        assert_eq!(some_ips_expected, some_ips);
    }

    /// This tests the timeline is computed correctly, when we are asked for the details.
    #[test]
    fn timeline() {
        let keeper = Keeper::dummy();
        let logger = libutils::test_logger();
        let mut last_end = 0;
        let mut intervals = Vec::new();
        for _ in 0..3 {
            let slice = dummy_time(last_end);
            let interval = slice.interval();
            last_end = flow::system_time_to_ms(interval.1);
            intervals.push(interval.0);
            intervals.push(interval.1);
            keeper.append(slice);
        }
        // Have only one instance of each border
        intervals = intervals
            .into_iter()
            .dedup()
            .collect();

        let run_query = |query: &Query| -> Response {
            let result = keeper
                .query(&logger, query)
                // We can afford to just .wait() on that future, since it is a fake one for now
                // (the result is ready right away).
                .wait()
                .unwrap();
            result.assert_sane();
            result
        };
        // If we don't ask for details, we get an unsplit timeslice
        let no_details = Query::default();
        let no_details_response = run_query(&no_details);
        assert!(no_details_response.timeline.is_unsplit());
        // But if we do, the timeline is split by all the intervals of the time slices
        let details = Query {
            details: true,
            ..Query::default()
        };
        let details_response = run_query(&details);
        let split_timeline = Timeline::from_borders(intervals.clone());
        assert_eq!(split_timeline, details_response.timeline);

        // Limit the query somewhat, so the first interval is cut off
        let limited = Query {
            details: true,
            start: Some(Time::Absolute(flow::system_time_from_ms(1))),
            ..Query::default()
        };
        let limited_response = run_query(&limited);
        intervals[0] = flow::system_time_from_ms(1);
        let limited_timeline = Timeline::from_borders(intervals);
        assert_eq!(limited_timeline, limited_response.timeline);
    }
}
