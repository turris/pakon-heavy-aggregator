// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

use std::cell::RefCell;
use std::rc::{Rc, Weak};

/// A trait to generalize things that need to be woken up sometime.
pub trait Wakeup {
    fn wakeup(&self);
}

/// A manager of wakeups.
///
/// Wakeup hungry objects can register here and they'd get woken up together. This is something
/// like a multiplexer for the wakeup signal.
#[derive(Clone)]
pub struct Manager {
    wakeups: Rc<RefCell<Vec<Weak<Wakeup>>>>,
}

impl Manager {
    /// Creates a new manager.
    pub fn new() -> Self {
        Self {
            wakeups: Rc::new(RefCell::new(Vec::new()))
        }
    }
    /// Wakes up all the registered objects.
    ///
    /// It also purges all the objects that are no longer alive.
    pub fn wakeup(&self) {
        self.wakeups
            .borrow_mut()
            .retain(|w| match w.upgrade() {
                Some(obj) => {
                    obj.wakeup();
                    true
                },
                None => false,
            });
    }
    /// Registers another object into the manager.
    ///
    /// Note that there's no unregister counter-part. The object gets unregistered automatically
    /// once the weak pointer gets dangling at it is attempted to be woken up.
    ///
    /// # Panics
    ///
    /// When trying to register something from within a wakeup caused by the same manager.
    pub fn register(&self, w: Weak<Wakeup>) {
        self.wakeups
            .borrow_mut()
            .push(w);
    }
}

#[cfg(test)]
mod tests {
    use std::cell::Cell;

    use super::*;

    /// Tests registering and waking up in the manager.
    #[test]
    fn manager() {
        impl Wakeup for Cell<bool> {
            fn wakeup(&self) {
                self.set(true);
            }
        }

        let w = Rc::new(Cell::new(false));
        let manager = Manager::new();
        assert_eq!(0, manager.wakeups.borrow().len());
        // Nothing bad happens if empty
        manager.wakeup();
        // Register something inside
        let weak = Rc::downgrade(&w);
        manager.register(weak);
        assert_eq!(1, manager.wakeups.borrow().len());
        assert!(!w.get());
        // Wake it up
        manager.wakeup();
        assert_eq!(1, manager.wakeups.borrow().len());
        assert!(w.get());
        // This kills the data, but doesn't unregister yet
        drop(w);
        assert_eq!(1, manager.wakeups.borrow().len());
        // But trying to wake it up does
        manager.wakeup();
        assert_eq!(0, manager.wakeups.borrow().len());
    }
}
