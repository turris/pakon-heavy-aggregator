// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

#[cfg(feature = "guts")]
extern crate dsrc_guts;
#[macro_use]
extern crate slog;
#[cfg(feature = "primary-name")]
extern crate int_compute_primary_name;
extern crate libaggregator;
extern crate libutils;

use std::env;
use std::process;

use libaggregator::reactor::Reactor;

/// Just `main`, but returning the exit code.
///
/// This is extracted outside of the real `main` so all the relevant destructors have a chance to
/// run. We do `process::exit` outside of it and that just terminates the process without
/// destructors. That may cause missing log messages.
fn do_main() -> i32 {
    let config = match libutils::cmdline_opts(env::args()) {
        Ok(config) => config,
        Err(e) => e.exit(),
    };
    let (logger, _guard) = libutils::logger(config.stderr_level, config.syslog_level);
    info!(logger, "Starting up");
    debug!(logger, "Command line options: {:?}", config);
    let reactor_run = Reactor::new(logger.new(o!("context" => "reactor")))
        .and_then(|mut reactor| {
            // Plug all available and configured data sources into the reactor and run it.
            let mut have_src = false;
            #[cfg(feature = "guts")]
            {
                if let Some(ref path) = config.guts {
                    reactor.add_dsrc(dsrc_guts::Guts::new(path.clone()));
                    have_src = true;
                }
            }
            if !have_src {
                warn!(logger, "No data source enabled, no data new will be gathered");
            }
            #[cfg(feature = "primary-name")]
            reactor.add_compute(int_compute_primary_name::Factory);
            reactor.run(config)
        });
    // Run everything and check how it went
    match reactor_run {
        Ok(()) => info!(logger, "Shutting down"),
        Err(e) => {
            crit!(logger, "{}", e);
            return 1;
        },
    };
    0
}

fn main() {
    let code = do_main();
    process::exit(code);
}
