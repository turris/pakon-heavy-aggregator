// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! This module provides various flow-related data types for reading the guts daemon snapshots.

use std::collections::{BTreeSet, HashMap};
use std::net::IpAddr;
use std::time::{Duration, SystemTime};
use std::ops;

use eui48::MacAddress;

use libdata::column::{Local, Remote, Tags, Type as ColumnType};
use libdata::flow::{self, Bytes, Count, Direction, IpProto, IpProtoRaw, Name, Port};
use libdata::stats::{Size, Sizes};
use libflow::update::{Key, Status, Update};

/// The current status of the flow
///
/// The status the flow is in this snapshot.
#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq)]
#[serde(rename_all = "snake_case")]
enum FlowStatus {
    /// A flow just born.
    ///
    /// This is the first snapshot related to the flow.
    New,
    /// The flow is in „normal“ state.
    ///
    /// The data flows and everyone is just happy.
    Ongoing,
    /// The flow closed in both directions.
    ///
    /// Note that not all flows need to go through this status, since not all protocols have a
    /// notion of closing (or we may not implement detection for it). Also, the flow can just
    /// silently die instead of closing (if an endpoint or both simply disappear) and then time
    /// out.
    Closed,
    /// This is the last snpashot of the flow.
    ///
    /// The guts daemon removes the flow from any internal tables and won't reference it in any way
    /// any more.
    Dropped,
}

/// An additional info about a DNS name.
///
/// It is empty for now, as the guts daemon doesn't provide the info yet. It'll be extended in the
/// future.
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq)]
struct NameInfo {}

/// An endpoint of the communication.
///
/// Contains all the relevant addresses.
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
struct Endpoint {
    /// The MAC (hardware) address.
    mac: Option<MacAddress>,
    /// IP (internet) address. Either IPv4 or IPv6.
    ip: IpAddr,
    /// The port (if applicable). Already in host byte order.
    port: Option<u16>,
    /// All the known DNS names for the IP address.
    name: HashMap<Name, NameInfo>,
}

/// A time in milliseconds since some time in the past.
///
/// This is what time the guts daemon gets and reports. It is its monotonic clock. Note that it
/// does not correspond to *our* monotonic clock (eg. `std::time::Instant`).
#[derive(Copy, Clone, Debug, Default, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct GutsTime(u64);

impl ops::Sub for GutsTime {
    type Output = Duration;
    fn sub(self, rhs: Self) -> Duration {
        Duration::from_millis(self.0 - rhs.0)
    }
}

impl ops::AddAssign<Duration> for GutsTime {
    fn add_assign(&mut self, rhs: Duration) {
        self.0 += rhs.as_secs() * 1000 + u64::from(rhs.subsec_nanos()) / 1_000_000;
    }
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
struct Id(String);

impl ColumnType for Id {
    fn name() -> String {
        "guts-flow-id".to_owned()
    }
}

/// Half of the flow in one direction.
///
/// It describes how much data and what happened so far and it is cummulative.
#[derive(Clone, Debug, Default, Deserialize, Eq, PartialEq)]
struct Half {
    /// Number of packets transmitted.
    packets: Count,
    /// Number of bytes in the payload part.
    ///
    /// This is in a sense the useful part of the packet. However, it is all the data inside
    /// UDP/TCP. There may be other wrappers around the actual data (like TLS) and the line is
    /// drawn in somewhat arbitrary place.
    payload: Bytes,
    /// Number of bytes including the network headers.
    ///
    /// This is including the IP headers (but not the link/ethernet headers). This is usually what
    /// the ISP charges.
    total: Bytes,
    /// Is the connection closed in the given direction?
    ///
    /// This changes to true once the direction is closed. Note that not all protocols support the
    /// notion of closing (TCP does, UDP does not, we may implement some notion of closing for some
    /// other protocols). Also, the connection may be closed in just one direction.
    closed: bool,
}

/// One flow snapshot.
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
pub struct Snapshot {
    /// ID of the flow
    ///
    /// So it can be tracked across snapshots and windows.
    id: Id,
    /// The current flow status.
    status: FlowStatus,
    /// The (wall) time when this flow started.
    #[serde(deserialize_with = "flow::system_time_deserialize")]
    starttime: SystemTime,
    /// The (never decreasing) time when this flow started.
    starttime_monotonic: GutsTime,
    /// The (wall) time when this flow received the last packet so far.
    #[serde(deserialize_with = "flow::system_time_deserialize")]
    lasttime: SystemTime,
    /// The (never decreasing) time when this flow received the last packet so far.
    lasttime_monotonic: GutsTime,
    /// The direction the flaw was initiated in.
    direction: Direction,
    // Note: We ignore the family in the input, as it can be deduced from the type of IP addresses
    /// The local endpoint of the communication (eg. in LAN).
    local: Endpoint,
    /// The remote endpoint of the communication (eg. somewhere on the Internet).
    remote: Endpoint,
    /// The protocol on top of IP.
    ip_proto: IpProto,
    /// The raw value of the protocol on top of IP, in case it is something strange.
    ip_proto_raw: u8,
    /// The data that flew inward.
    #[serde(rename = "in")]
    in_half: Half,
    /// The data that flew outward.
    #[serde(rename = "out")]
    out_half: Half,
}

impl From<Snapshot> for Update {
    fn from(snapshot: Snapshot) -> Update {
        let status = match snapshot.status {
            FlowStatus::New => Status::Start,
            FlowStatus::Ongoing => Status::Ongoing,
            FlowStatus::Closed |
            FlowStatus::Dropped => Status::End,
        };
        fn sizes(half: &Half) -> Size {
            Size {
                packets: half.packets,
                size: half.total,
            }
        }
        let mut t = Tags::new();
        t.insert(snapshot.direction);
        t.insert(snapshot.ip_proto);
        t.insert(IpProtoRaw(snapshot.ip_proto_raw));
        macro_rules! enp {
            ($endp: expr, $modif: ident) => {
                if let Some(mac) = $endp.mac {
                    t.insert($modif(mac));
                }
                t.insert($modif($endp.ip));
                if let Some(port) = $endp.port {
                    t.insert($modif(Port(port)));
                }
                let names = $endp.name
                    .keys()
                    .cloned()
                    .collect::<BTreeSet<_>>();
                t.insert($modif(names));
            }
        }
        enp!(snapshot.local, Local);
        enp!(snapshot.remote, Remote);
        Update {
            keys: vec![
                Key::Simple(snapshot.id.into()),
                Key::FlowTuple {
                    ip_proto_raw: snapshot.ip_proto_raw,
                    loc_ip: snapshot.local.ip,
                    rem_ip: snapshot.remote.ip,
                    loc_port: snapshot.local.port,
                    rem_port: snapshot.remote.port,
                },
            ],
            status,
            tags: t,
            stats: Some(Sizes {
                dir_in: sizes(&snapshot.in_half),
                dir_out: sizes(&snapshot.out_half),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use serde_json;

    use super::*;
    use libdata::flow::{self, Name};
    use test_help;

    /// Envelope to get rid of the JSONRPC stuff.
    #[derive(Debug, Deserialize, Eq, PartialEq)]
    struct Envelope {
        /// The actual data we are interested in.
        params: Snapshot,
    }

    struct TestData {
        id: Id,
        loc_ip: IpAddr,
        rem_ip: IpAddr,
        rem_names: HashMap<Name, NameInfo>,
        rem_mac: MacAddress,
    }

    impl TestData {
        fn new() -> Self {
            let rem_names = [
                    "vorner.cz",
                    "lair.vorner.cz",
                ]
                .into_iter()
                .map(|n| (Name((*n).to_owned()), NameInfo {}))
                .collect();
            Self {
                id: Id("1490683648386-354867-140629560518400-595".to_owned()),
                loc_ip: "2001:1488:fffe:6005:a545:b284:9b00:b155".parse().unwrap(),
                rem_ip: "2a02:2b88:0002:0001:0000:0000:10b3:0001".parse().unwrap(),
                rem_names,
                rem_mac: "d8:58:d7:00:19:d6".parse().unwrap(),
            }
        }
    }

    /// Check we can decode the structure from the json.
    ///
    /// We reuse the complete JSONRPC message and throw away the envelope by providing a wrapper
    /// structure, to save us the need to have another copy of the (rather large) JSON string.
    ///
    /// We also check the snapshot can be converted to some more useful bits.
    #[test]
    fn snapshot_decode() {
        let data = TestData::new();
        let envelope = serde_json::from_str::<Envelope>(test_help::ONE_SNAPSHOT).unwrap();
        assert_eq!(envelope.params, Snapshot {
            id: data.id,
            status: FlowStatus::Ongoing,
            starttime: flow::system_time_from_ms(1490683648386),
            starttime_monotonic: GutsTime(354867331),
            lasttime: flow::system_time_from_ms(1490703793562),
            lasttime_monotonic: GutsTime(375012507),
            direction: Direction::Out,
            local: Endpoint {
                ip: data.loc_ip,
                name: HashMap::new(),
                port: Some(52250),
                mac: None,
            },
            remote: Endpoint {
                ip: data.rem_ip,
                name: data.rem_names,
                port: Some(22),
                mac: Some(data.rem_mac),
            },
            ip_proto: IpProto::Tcp,
            ip_proto_raw: 6,
            in_half: Half {
                packets: Count(41419),
                payload: Bytes(13435427),
                total: Bytes(16417603),
                closed: false,
            },
            out_half: Half {
                packets: Count(35910),
                payload: Bytes(562792),
                total: Bytes(3148320),
                closed: false,
            },
        });
    }

    /// Test converting a snapshot to an update
    #[test]
    fn to_update() {
        let data = TestData::new();
        let envelope = serde_json::from_str::<Envelope>(test_help::ONE_SNAPSHOT).unwrap();
        let update = Update::from(envelope.params);
        let mut tags = Tags::new();
        tags.insert(Direction::Out);
        tags.insert(IpProto::Tcp);
        tags.insert(IpProtoRaw(6));
        tags.insert(Local(data.loc_ip));
        tags.insert(Local(Port(52250)));
        tags.insert(Local(BTreeSet::<Name>::new()));
        tags.insert(Remote(data.rem_ip));
        tags.insert(Remote(Port(22)));
        let names = data.rem_names
            .into_iter()
            .map(|(n, _)| n)
            .collect::<BTreeSet<_>>();
        tags.insert(Remote(names));
        tags.insert(Remote(data.rem_mac));
        let stats = Sizes {
            dir_in: Size {
                packets: Count(41419),
                size: Bytes(16417603),
            },
            dir_out: Size {
                packets: Count(35910),
                size: Bytes(3148320),
            },
        };
        let expected = Update {
            keys: vec![
                Key::Simple(data.id.into()),
                Key::FlowTuple {
                    ip_proto_raw: 6,
                    loc_ip: data.loc_ip,
                    rem_ip: data.rem_ip,
                    loc_port: Some(52250),
                    rem_port: Some(22),
                },
            ],
            tags,
            status: Status::Ongoing,
            stats: Some(stats),
        };
        assert_eq!(expected, update);
    }
}
