// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! The server logic of the guts connection.
//!
//! This takes the raw connection to the upstream (the guts daemon) and produces a stream of
//! snapshots for the rest of the system to handle.

use std::cell::RefCell;
use std::io::Error as IoError;
use std::rc::Rc;

use futures::{Future, IntoFuture, Sink};
use slog::Logger;
use serde_json::Value;
use tokio_core::reactor::Handle;
use tokio_jsonrpc::codec::DirtyLine;
use tokio_jsonrpc::{Endpoint, RpcError, Server, ServerCtl};
use tokio_jsonrpc::server::BoxNotificationResult;
use tokio_io::{AsyncRead, AsyncWrite};

use libflow::update::{CorkedUpdate, UpdateSender};
use libutils::LocalBoxFuture;

use snapshot::Snapshot;

/// The server that accepts notifications from guts uplink and handles them.
struct UpstreamServer {
    /// The logger used internally.
    logger: Logger,
    /// Due to the way how sending works, we need to extract the sender and return it back once
    /// sending is complete. This structure allows that.
    sender: Rc<RefCell<Option<UpdateSender>>>,
}

impl Server for UpstreamServer {
    // As we don't implement any RPCs, these don't matter, but we have to declare them anyway
    type Success = ();
    type RpcCallResult = Result<(), RpcError>;
    type NotificationResult = BoxNotificationResult;
    fn notification(&self,
                    ctl: &ServerCtl,
                    method: &str,
                    params: &Option<Value>)
        -> Option<BoxNotificationResult>
    {
        match method {
            "pakon.proto-version" => {
                // Try to decode the parameters
                let result = jsonrpc_params!(params, wrap "version" => u32)
                    .map_err(|e| {
                        error!(self.logger, "Couldn't decode version parameters: {}", e.message);
                    })
                    // If we got the version correctly, report it and check we support it
                    .and_then(|(version,)| {
                        match version {
                            1 => {
                                debug!(self.logger, "Received an upstream version {}", version);
                                Ok(())
                            },
                            other => {
                                error!(self.logger, "Unsupported upstream version {}", other);
                                ctl.terminate();
                                Err(())
                            },
                        }
                    });
                Some(Box::new(result.into_future()))
            },
            "pakon.flow-update" => {
                let snapshot = match jsonrpc_params!(params, wrap "snapshot" => Snapshot) {
                    Ok((snapshot,)) => snapshot,
                    Err(e) => {
                        error!(self.logger, "Couldn't decode snapshot: {}", e.message);
                        return Some(Box::new(Err(()).into_future()));
                    },
                };
                trace!(self.logger, "Received a flow snapshot {:?}", snapshot);
                // This is a little dance to make sending into bounded channel work. We take the
                // sender out of the Option, do the sending and then when successful, we return it
                // back. We are guaranteed not to be called in the meantime, because we don't
                // configure the server to allow parallel processing.
                let return_cell = Rc::clone(&self.sender);
                let logger = self.logger.clone();
                let ctl = ctl.clone();
                let sender = self.sender
                    .borrow_mut()
                    .take()
                    .expect("Someone didn't return the sender");
                let result = sender
                    .send(CorkedUpdate::Update(snapshot.into()))
                    .map(move |sender| {
                        *return_cell.borrow_mut() = Some(sender);
                    })
                    .map_err(move |e| {
                        // The error likely means the other end of the channel has disappeared.
                        // While this shouldn't happen in practice, we have no better way to
                        // continue locally than to terminate the connection and give up.
                        error!(logger, "Failed to forward the report: {}", e);
                        ctl.kill();
                        // Implicitly return () as the error type.
                    });
                Some(Box::new(result))
            },
            _ => {
                warn!(self.logger, "Unknown upstream method {}", method);
                None
            },
        }
    }
}

/// Wraps the connection and starts sending the updates into the sender.
///
/// The updates are flushed into the sink.
pub fn convert<C>(logger: &Logger,
                  handle: &Handle,
                  connection: C,
                  sender: UpdateSender)
    -> LocalBoxFuture<(), IoError>
where
    C: AsyncRead + AsyncWrite + Send + 'static
{
    debug!(logger, "Connected to upstream, converting it to snapshot");
    let server = UpstreamServer {
        logger: logger.new(o!("context" => "guts reportsrv")),
        sender: Rc::new(RefCell::new(Some(sender))),
    };
    let (_client, terminated) = Endpoint::new(connection.framed(DirtyLine::new()), server)
        .logger(logger.clone())
        .start(handle);
    Box::new(terminated)
}

#[cfg(test)]
mod tests {
    use futures::{Future, Stream};
    use futures::unsync::mpsc;
    use tokio_core::reactor::Core;
    use tokio_io::io;
    use tokio_uds::UnixStream;

    use super::*;
    use libflow::update::CorkedUpdate;
    use libutils;
    use test_help::ONE_SNAPSHOT;

    /// Feed the convertor some data and return whatever results it produced.
    fn feed(data: &[u8]) -> Vec<CorkedUpdate> {
        let mut core = Core::new().unwrap();
        let handle = core.handle();
        let (stream_snd, stream_rcv) = UnixStream::pair(&handle).unwrap();
        let (channel_snd, channel_rcv) = mpsc::channel(5);
        let logger = libutils::test_logger();
        let convert_future = convert(&logger, &handle, stream_rcv, channel_snd);
        // Write all the data
        let written = io::write_all(stream_snd, data)
            // Flush the stream
            .and_then(|(stream, _)| io::flush(stream))
            // And throw the stream away, closing it in the meantime
            .map(|_| ());
        // Collect all the (future) snapshots into a vector
        let result = channel_rcv.collect().map_err(|_| panic!());
        let all = result.join3(convert_future, written).map(|(vec, _, _)| vec);
        core.run(all).unwrap()
    }

    /// Tests the convert gives us snapshots over the channel when we feed it with a stream. Also
    /// checks it correctly terminates when it loses the connection.
    #[test]
    fn basic_convert() {
        let snapshots = feed(ONE_SNAPSHOT.as_bytes());
        assert_eq!(1, snapshots.len());
    }

    /// Test we accept the version 1 protocol.
    ///
    /// We test it by passing a snapshot after the version and checking it gets out.
    #[test]
    fn accept_version() {
        let version = concat!(r#"{"jsonrpc":"2.0","method":"pakon.proto-version","#,
                              r#""params":{"version":1}}"#,
                              "\n");
        let input = version.to_owned() + ONE_SNAPSHOT;
        let snapshots = feed(input.as_bytes());
        assert_eq!(1, snapshots.len());
    }

    /// Test we don't accept version 2 protocol.
    ///
    /// The connection gets terminated and no snapshots get out (note that this test implicitly
    /// assumes the whole data will fit into a kernel buffer, so we don't get an EPIPE or
    /// something. If that's not the case, the test will have to get fixed, but currently it
    /// works).
    #[test]
    fn refuse_version() {
        let version = concat!(r#"{"jsonrpc":"2.0","method":"pakon.proto-version","#,
                              r#""params":{"version":2}}"#,
                              "\n");
        let input = version.to_owned() + ONE_SNAPSHOT;
        let snapshots = feed(input.as_bytes());
        assert_eq!(0, snapshots.len());
    }
}
