// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! Some support for unit tests, like test data.

#![deny(missing_docs)]

extern crate serde;
extern crate serde_json;

use std::fmt::Debug;

use serde::{Deserialize, Serialize};
use serde_json::Value;

/// A single snapshot (taken directly from the guts' output).
pub const ONE_SNAPSHOT: &str = concat!(r#"{"jsonrpc": "2.0", "method": "#,
            r#""pakon.flow-update","params":{"verdict":{"action":"ACCEPT"},"id":"#,
            r#""1490683648386-354867-140629560518400-595","status":"ongoing","#,
            r#""starttime":1490683648386,"starttime_monotonic":354867331,"lasttime""#,
            r#":1490703793562,"lasttime_monotonic":375012507,"direction":"OUT","family""#,
            r#":"IPv6","local":{"ip":"2001:1488:fffe:6005:a545:b284:9b00:b155","name":"#,
            r#"{},"port":52250},"remote":{"ip":"#,
            r#""2a02:2b88:0002:0001:0000:0000:10b3:0001","name":{"vorner.cz":{},"#,
            r#""lair.vorner.cz":{}},"mac":"d8:58:d7:00:19:d6","port":22},"ip_proto""#,
            r#":"TCP","ip_proto_raw":6,"in":{"packets":41419,"payload":13435427,"#,
            r#""total":16417603,"closed":false},"out":{"packets":35910,"payload":"#,
            r#"562792,"total":3148320,"closed":false},"flags":{"src":"eth0","qdir":"out"}}}"#,
            "\n");

/// An empty query string.
pub const EMPTY_QUERY: &str = concat!(r#"{"jsonrpc":"2.0","method":"query","params":{},"id":1}"#,
                                      "\n");
/// An empty repeated query string.
pub const EMPTY_REPEATED: &str = concat!(r#"{"jsonrpc":"2.0","method":"repeated","id":2,"#,
                                         r#""params":{"id":"repeated","query":{}}}"#, "\n");

/// Checks that something deserialized correctly.
pub fn deser<'de, T: Deserialize<'de> + Eq + Debug>(json: &'de str, expected: &T) {
    let deserialized: T = serde_json::from_str(json).unwrap();
    assert_eq!(&deserialized, expected);
}

/// Checks something serializes as expected, using a json Value to compare it.
pub fn ser_json<T: Serialize>(json: &Value, value: &T) {
    let value_encoded = serde_json::to_value(value).unwrap();
    assert_eq!(json, &value_encoded);
}

/// Checks that something serializes correctly.
pub fn ser<T: Serialize>(json: &str, value: &T) {
    let json_decoded = serde_json::from_str::<Value>(json).unwrap();
    ser_json(&json_decoded, value);
}

