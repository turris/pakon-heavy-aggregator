// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! Handling of flows and related data structures.
//!
//! This module (and its submodules) knows how flows, flow slices and such look like and how to
//! take care of them. These are mostly data structures and they have to be orchestrated by
//! something else (eg. an event loop).

#![deny(missing_docs)]

extern crate futures;
extern crate libdata;
extern crate libquery;
extern crate libutils;
#[cfg(test)]
extern crate tokio_core;
extern crate void;

pub mod slice;
pub mod update;
