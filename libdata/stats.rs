// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! The statistics about a flow or a group of flows.
//!
//! These represent the numerical values of a flow ‒ sizes, times, speeds, etc.

use std::cmp;
use std::time::{Duration, SystemTime};

use serde::{Serialize, Serializer};
use serde::ser::SerializeMap;

use flow::{self, Bytes, Count, Speed};

/// A poorer version of [`Stat`](struct.Stat.html)
///
/// This one contains only the number of packets and the size. It is used during the construction
/// of the flow slice and the other statistics are added later on.
///
/// Because the duration and the fact it relates to a single flow is known during the conversion,
/// the other statistics are not needed and this makes it easier to manipulate.
#[derive(Add, AddAssign, Clone, Debug, Default, Eq, PartialEq, Sub)]
pub struct Size {
    /// Number of packets.
    pub packets: Count,
    /// Number of bytes (including headers).
    pub size: Bytes,
}

impl Size {
    /// Expands into full [`Stat`](struct.Stat.html)
    ///
    /// # Params
    ///
    /// * `start`: The absolute time when the flow slice starts. This is either start of the slice,
    ///    or start of the flow if it starts inside the slice.
    /// * `end`: The opposite of `start`.
    /// * `speed_duration`: The duration of the flow slice used for speed computation. Due to some
    ///    issues with summing maximal and average speeds, this is not always the same as `end -
    ///    start`.
    /// * `started`: Did the flow start in this slice?
    fn expand(&self, start: SystemTime, end: SystemTime, speed_duration: Duration, started: bool)
        -> Stat
    {
        Stat {
            packets: self.packets,
            size: self.size,
            max_speed: Speed::compute(self.size, speed_duration),
            speed_duration,
            flows: Count(1),
            flows_started: Count(if started { 1 } else { 0 }),
            start: Some(start),
            end: Some(end),
        }
    }
    /// Checks if other is equal or smaller in each subfield.
    ///
    /// This basically checks if it is safe to do `self - other`.
    pub fn sub_safe(&self, other: &Size) -> bool {
        self.packets >= other.packets && self.size >= other.size
    }
}

/// A poorer version of [`Stats`](struct.Stats.html)
///
/// This one contains only the number of packets and the size. It is used during the construction
/// of the flow slice and the other statistics are added later on.
///
/// Because the duration and the fact it relates to a single flow is known during the conversion,
/// the other statistics are not needed and this makes it easier to manipulate.
#[derive(Add, AddAssign, Clone, Debug, Default, Eq, PartialEq, Sub)]
pub struct Sizes {
    /// The inbound direction.
    pub dir_in: Size,
    /// The outbound direction.
    pub dir_out: Size,
}

impl Sizes {
    /// Expands into full [`Stats`](struct.Stats.html)
    ///
    /// # Params
    ///
    /// * `start`: The absolute time when the flow slice starts. This is either start of the slice,
    ///    or start of the flow if it starts inside the slice.
    /// * `end`: The opposite of `start`.
    /// * `speed_duration`: The duration of the flow slice used for speed computation. Due to some
    ///    issues with summing maximal and average speeds, this is not always the same as `end -
    ///    start`.
    /// * `started`: Did the flow start in this slice?
    pub fn expand(&self, start: SystemTime, end: SystemTime, speed_duration: Duration,
                  started: bool)
        -> Stats
    {
        Stats {
            dir_in: self.dir_in.expand(start, end, speed_duration, started),
            dir_out: self.dir_out.expand(start, end, speed_duration, started),
        }
    }
    /// Checks if other is equal or smaller in each subfield.
    ///
    /// This basically checks if it is safe to do `self - other`.
    pub fn sub_safe(&self, other: &Sizes) -> bool {
        self.dir_in.sub_safe(&other.dir_in) && self.dir_out.sub_safe(&other.dir_out)
    }
}

/// The flow statistics in a single direction.
///
/// This specifies the statistics (sizes, etc.) in a single direction. This refers to a single
/// interval of the corresponding time line.
///
/// Note that this computes some derived values during serialization and omits others (eg. duration
/// is not present, but an average speed is added).
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Stat {
    /// How many packets were transferred through all the relevant flows.
    pub packets: Count,
    /// How many bytes there were, including the headers.
    pub size: Bytes,
    /// A maximal speed of transfer.
    ///
    /// This is the peek ‒ when we aggregate multiple smaller intervals together, this one picks
    /// the largest one of them.
    pub max_speed: Speed,
    /// The time duration of this statistics.
    ///
    /// Used internally for speed computations when combining together.
    pub speed_duration: Duration,
    /// How many flows are present in this slice.
    pub flows: Count,
    /// How many flows started in this slice.
    ///
    /// This is included in `flows` above.
    pub flows_started: Count,
    /// When this flow slice started.
    ///
    /// This is either the time the flow started (if it is in the current time slice) or the time
    /// when the time slice started.
    ///
    /// Not present if there are no flows inside.
    pub start: Option<SystemTime>,
    /// When this flow slice ended.
    ///
    /// This is the opposite side than `start`.
    ///
    /// Not present if there are no flows inside.
    pub end: Option<SystemTime>,
}

impl Stat {
    /// Is it empty?
    ///
    /// The default (all-zeroes) value is considered empty.
    pub fn is_empty(&self) -> bool {
        self.flows == Count(0)
    }
    fn combine_ends(&mut self, other: &Self) {
        fn combine<F>(f: F, t1: &Option<SystemTime>, t2: &Option<SystemTime>) -> Option<SystemTime>
        where
            F: FnOnce(SystemTime, SystemTime) -> SystemTime
        {
            match (*t1, *t2) {
                (None, None) => None,
                (Some(v), None) |
                (None, Some(v)) => Some(v),
                (Some(v1), Some(v2)) => Some(f(v1, v2)),
            }
        }
        self.start = combine(cmp::min, &self.start, &other.start);
        self.end = combine(cmp::max, &self.end, &other.end);
    }
    /// Joins the stats over each other.
    ///
    /// It assumes they happen during the same time.
    pub fn combine_overlay(&mut self, other: &Self) {
        self.packets += other.packets;
        self.size += other.size;
        // As they happen at the same time, we sum the max speeds together
        self.max_speed += other.max_speed;
        // But the duration doesn't sum up, for obvious reasons.
        self.speed_duration = cmp::max(self.speed_duration, other.speed_duration);
        self.flows += other.flows;
        self.flows_started += other.flows_started;
        self.combine_ends(other);
    }
    /// Joins the stats side by side.
    ///
    /// Unlike [`combine_overlay`](#method.combine_overlay), this acts as joining them sequentially. The
    /// difference is on the speeds.
    ///
    /// Also, this assumes self is sooner in the time than the other (eg. self happened before
    /// other).
    pub fn combine_seq(&mut self, other: &Self) {
        self.packets += other.packets;
        self.size += other.size;
        self.max_speed = cmp::max(self.max_speed, other.max_speed);
        self.speed_duration += other.speed_duration;
        self.flows_started += other.flows_started;
        if self.flows == Count(0) {
            // If we are an empty bucket, there was never anything inside. So all the flows that
            // are present in other are new ones and we should count them all.
            self.flows = other.flows;
        } else {
            // There was a real bucket to the left of other. Therefore, all the flows that don't
            // start in other are already accounted for inside us and we want to add only the new
            // ones.
            self.flows += other.flows_started;
        }
        self.combine_ends(other);
    }
}

impl Serialize for Stat {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        // If some fields are added, modify the size hint here!
        let mut map = serializer.serialize_map(Some(7))?;
        map.serialize_entry("packets", &self.packets)?;
        map.serialize_entry("size", &self.size)?;
        map.serialize_entry("avg-speed", &Speed::compute(self.size, self.speed_duration))?;
        map.serialize_entry("max-speed", &self.max_speed)?;
        map.serialize_entry("flows", &self.flows)?;
        // We probably could omit these if they are null. But in practice, if they are null, the
        // whole Stat is empty and omited altogether from inside the `Stats`, so it doesn't matter.
        map.serialize_entry("start", &self.start.map(flow::system_time_to_ms))?;
        map.serialize_entry("end", &self.end.map(flow::system_time_to_ms))?;
        map.end()
    }
}

/// The flow statistics in both directions.
///
/// This is simply a pair of in and out directions.
///
/// In case a direction is empty (eg. all zeroes), it is omitted from the serialized result, to
/// avoid unnecessary zero-clutter. Therefore, if some interval was „calm“ (no traffic), an empty
/// dict is serialized.
#[derive(Clone, Debug, Default, Eq, PartialEq, Serialize)]
pub struct Stats {
    /// Statistics in the inbound direction.
    #[serde(rename = "in", skip_serializing_if = "Stat::is_empty")]
    pub dir_in: Stat,
    /// Statistics in the outbound direction.
    #[serde(rename = "out", skip_serializing_if = "Stat::is_empty")]
    pub dir_out: Stat,
}

impl Stats {
    /// Joins the stats over each other.
    ///
    /// It assumes they happen during the same time.
    pub fn combine_overlay(&mut self, other: &Self) {
        self.dir_in.combine_overlay(&other.dir_in);
        self.dir_out.combine_overlay(&other.dir_out);
    }
    /// Joins the stats side by side.
    ///
    /// Unlike [`combine_overlay`](#method.combine_overlay), this acts as joining them sequentially. The
    /// difference is on the speeds.
    pub fn combine_seq(&mut self, other: &Self) {
        self.dir_in.combine_seq(&other.dir_in);
        self.dir_out.combine_seq(&other.dir_out);
    }
}

#[cfg(test)]
mod tests {
    use std::time::{Duration, UNIX_EPOCH};

    use super::*;
    use test_help;

    /// A fixture for several stats based tests.
    pub struct StatTests {
        pub s1: Stats,
        pub s2: Stats,
        pub exp_overlay: Stats,
        pub exp_seq: Stats,
    }

    impl StatTests {
        /// Creates the fixture and generates some test data that can be reused.
        pub fn new() -> Self {
            // Note that in practice the count of flows in both directions is the same.
            // However, for better tests we let them be different here.
            let s1 = Stats {
                dir_in: Stat {
                    packets: Count(2),
                    size: Bytes(1300),
                    max_speed: Speed(1300),
                    speed_duration: Duration::from_secs(10),
                    flows: Count(2),
                    flows_started: Count(1),
                    start: Some(UNIX_EPOCH),
                    end: Some(UNIX_EPOCH + Duration::from_secs(10)),
                },
                dir_out: Stat {
                    packets: Count(1),
                    size: Bytes(100),
                    max_speed: Speed(50),
                    speed_duration: Duration::from_secs(6),
                    flows: Count(4),
                    flows_started: Count(0),
                    start: Some(UNIX_EPOCH + Duration::from_secs(4)),
                    end: Some(UNIX_EPOCH + Duration::from_secs(10)),
                },
            };
            let s2 = Stats {
                dir_in: Stat {
                    packets: Count(3),
                    size: Bytes(1500),
                    max_speed: Speed(500),
                    speed_duration: Duration::from_secs(5),
                    flows: Count(3),
                    flows_started: Count(2),
                    start: Some(UNIX_EPOCH + Duration::from_secs(5)),
                    end: Some(UNIX_EPOCH + Duration::from_secs(10)),
                },
                dir_out: Stat {
                    packets: Count(2),
                    size: Bytes(400),
                    max_speed: Speed(150),
                    speed_duration: Duration::from_secs(6),
                    flows: Count(4),
                    flows_started: Count(2),
                    start: Some(UNIX_EPOCH + Duration::from_secs(5)),
                    end: Some(UNIX_EPOCH + Duration::from_secs(11)),
                },
            };
            let exp_overlay = Stats {
                dir_in: Stat {
                    packets: Count(5),
                    size: Bytes(2800),
                    max_speed: Speed(1800),
                    speed_duration: Duration::from_secs(10),
                    flows: Count(5),
                    flows_started: Count(3),
                    start: Some(UNIX_EPOCH),
                    end: Some(UNIX_EPOCH + Duration::from_secs(10)),
                },
                dir_out: Stat {
                    packets: Count(3),
                    size: Bytes(500),
                    max_speed: Speed(200),
                    speed_duration: Duration::from_secs(6),
                    flows: Count(8),
                    flows_started: Count(2),
                    start: Some(UNIX_EPOCH + Duration::from_secs(4)),
                    end: Some(UNIX_EPOCH + Duration::from_secs(11)),
                },
            };
            let exp_seq = Stats {
                dir_in: Stat {
                    packets: Count(5),
                    size: Bytes(2800),
                    max_speed: Speed(1300),
                    speed_duration: Duration::from_secs(15),
                    flows: Count(4),
                    flows_started: Count(3),
                    start: Some(UNIX_EPOCH),
                    end: Some(UNIX_EPOCH + Duration::from_secs(10)),
                },
                dir_out: Stat {
                    packets: Count(3),
                    size: Bytes(500),
                    max_speed: Speed(150),
                    speed_duration: Duration::from_secs(12),
                    flows: Count(6),
                    flows_started: Count(2),
                    start: Some(UNIX_EPOCH + Duration::from_secs(4)),
                    end: Some(UNIX_EPOCH + Duration::from_secs(11)),
                },
            };
            Self {
                s1,
                s2,
                exp_overlay,
                exp_seq,
            }
        }
    }

    /// Tests combining multiple stats together.
    #[test]
    fn stat_combine() {
        let data = StatTests::new();
        // Test the overlay way (eg. happening at the same time)
        // Test it both ways, to make sure it doesn't changes anything
        let mut overlay = data.s1.clone();
        overlay.combine_overlay(&data.s2);
        assert_eq!(data.exp_overlay, overlay);
        overlay = data.s2.clone();
        overlay.combine_overlay(&data.s1);
        assert_eq!(data.exp_overlay, overlay);
        // Test the sequential way (happening at different time).
        let mut seq = data.s1.clone();
        seq.combine_seq(&data.s2);
        assert_eq!(data.exp_seq, seq);
        // However, the sequential combining *is* order sensitive, so don't try the other way.
    }

    /// Tests the serialization of the statistics
    #[test]
    fn stat_ser() {
        let data = StatTests::new();
        // An empty statistics produces an empty object
        test_help::ser("{}", &Stats::default());
        // But empty Stat (single one) produces something
        let empty_stat = json!({
            "packets": 0,
            "size": 0,
            "avg-speed": 0,
            "max-speed": 0,
            "flows": 0,
            "start": null,
            "end": null
        });
        test_help::ser_json(&empty_stat, &Stat::default());
        let stats_exp = json!({
            "in": {
                "packets": 2,
                "size": 1300,
                "avg-speed": 130,
                "max-speed": 1300,
                "flows": 2,
                "start": 0,
                "end": 10000
            },
            "out": {
                "packets": 1,
                "size": 100,
                "avg-speed": 17,
                "max-speed": 50,
                "flows": 4,
                "start": 4000,
                "end": 10000
            }
        });
        test_help::ser_json(&stats_exp, &data.s1);
    }

    /// Test expansion of the `Size` and `Sizes` structures.
    #[test]
    fn stats_expand() {
        let s_in = Size {
            packets: Count(10),
            size: Bytes(1024),
        };
        let start = UNIX_EPOCH + Duration::from_secs(5);
        let end = UNIX_EPOCH + Duration::from_secs(15);
        let e_in = s_in.expand(start, end, Duration::from_secs(5), true);
        let exp_in = Stat {
            packets: Count(10),
            size: Bytes(1024),
            start: Some(start),
            end: Some(end),
            flows: Count(1),
            flows_started: Count(1),
            speed_duration: Duration::from_secs(5),
            max_speed: Speed(205),
        };
        assert_eq!(exp_in, e_in);
    }

    /// Tests the substracting guard tests.
    #[test]
    fn sub_safe() {
        let a = Size {
            packets: Count(10),
            size: Bytes(1024),
        };
        let b = Size {
            packets: Count(12),
            size: Bytes(2048),
        };
        let c = Size {
            packets: Count(9),
            size: Bytes(2048),
        };
        assert!(a.sub_safe(&a));
        assert!(!a.sub_safe(&b));
        assert!(b.sub_safe(&a));
        assert!(!a.sub_safe(&c));
        assert!(!c.sub_safe(&a));
        let aa = Sizes {
            dir_in: a.clone(),
            dir_out: a.clone(),
        };
        let bb = Sizes {
            dir_in: b.clone(),
            dir_out: b.clone(),
        };
        let ab = Sizes {
            dir_in: a.clone(),
            dir_out: b.clone(),
        };
        assert!(aa.sub_safe(&aa));
        assert!(!aa.sub_safe(&bb));
        assert!(bb.sub_safe(&aa));
        assert!(!aa.sub_safe(&ab));
        assert!(!ab.sub_safe(&bb));
        assert!(bb.sub_safe(&ab));
        assert!(ab.sub_safe(&aa));
    }
}
