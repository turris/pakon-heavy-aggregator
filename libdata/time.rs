// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.
//

//! Data structures to work with the time and split it into a timeline.
//!
//! The [`Timeline`](struct.Timeline.html) is a time interval subdivided into smaller intervals.
//! However, these smaller intervals don't have to be the same length.
//!
//! There's already the [`Time`](struct.Time.hmtl) structure, which allows addressing an instant in
//! time in absolute and relative way.

use std::slice::Iter;
use std::time::{Duration, SystemTime};

use serde::{Deserialize, Deserializer};

use itertools::Itertools;
use serde::{Serialize, Serializer};
use serde::ser::SerializeSeq;

use flow;

/// An iterator over the intervals of a [`Timeline`](struct.Timeline.html).
#[derive(Clone, Debug)]
pub struct IntervalIter<'a> {
    iter: Iter<'a, SystemTime>,
    previous: Option<SystemTime>,
    exhausted: bool,
}

impl<'a> Iterator for IntervalIter<'a> {
    type Item = (Option<SystemTime>, Option<SystemTime>);
    fn next(&mut self) -> Option<Self::Item> {
        if self.exhausted {
            None
        } else {
            let previous = self.previous;
            self.previous = self.iter.next().cloned();
            self.exhausted = self.previous.is_none();
            Some((previous, self.previous))
        }
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let (low, high) = self.iter.size_hint();
        (low + 1, high.map(|h| h + 1))
    }
}

/// A timeline.
///
/// A timeline splits the time continuum into intervals. There are borders (the times when one
/// interval ends and another begins) and intervals. Note that there's one more of the intervals
/// than the borders and the first and last intervals are infinite (eg. the first one is from -∞,
/// the last one to ∞).
///
/// # Note
///
/// This structure doesn't provide the usual methods like `len` or `into_iter`. The reason is, as
/// it can be iterated in two different ways, such methods would be ambiguous and therefore
/// confusing. It has [`borders`](#method.borders) and [`intervals`](#method.intervals) instead.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Timeline {
    /// The storage of the current borders.
    ///
    /// Note that we store only the borders, not the intervals. The intervals can be computed.
    borders: Vec<SystemTime>,
}

impl Timeline {
    /// Creates a new empty timeline.
    ///
    /// The newly created timeline has no borders and only a single interval (-∞, ∞).
    pub fn new() -> Self {
        Self {
            borders: Vec::new(),
        }
    }

    /// Panics if the internal invariants are broken.
    fn assert_sane(&self) {
        let sane = self.borders
            .iter()
            .tuple_windows()
            .all(|(a, b)| a < b);
        assert!(sane, "The borders either aren't sorted or there's a duplicate border");
    }

    /// Creates a timeline from a vec of borders.
    ///
    /// # Parameters
    ///
    /// * `borders`: A sorted array of (unique) borders between the intervals in the timeline to
    ///   be.
    ///
    /// # Panics
    ///
    /// The consructor panics if the provided list of borders is not sorted or if there are
    /// duplicates.
    pub fn from_borders(borders: Vec<SystemTime>) -> Self {
        let timeline = Self {
            borders
        };
        timeline.assert_sane();
        timeline
    }

    /// Provides the count of borders between intervals.
    pub fn border_count(&self) -> usize {
        self.borders.len()
    }

    /// Provides the count of intervals.
    ///
    /// Note that this is always [`border_count()`](#method.border_count) + 1.
    pub fn interval_count(&self) -> usize {
        self.border_count() + 1
    }

    /// Returns an iterator over the borders.
    pub fn borders(&self) -> Iter<SystemTime> {
        self.borders.iter()
    }

    /// Returns an iterator over the intervals.
    ///
    /// The first interval starts in -∞ (specified by `None`) and the last one ends with ∞ (also
    /// specified by `None`). If the interval has a neighbor, it has `Some(border)` towards the
    /// neighbor.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use libdata::time::Timeline;
    /// let empty = Timeline::new();
    /// let intervals = empty.intervals().collect::<Vec<_>>();
    /// assert_eq!(vec![(None, None)], intervals);
    /// ```
    ///
    /// ```rust
    /// # use std::time::SystemTime;
    /// # use libdata::time::Timeline;
    /// let mut timeline = Timeline::new();
    /// let time = SystemTime::now();
    /// timeline.split(time);
    /// let intervals = timeline.intervals().collect::<Vec<_>>();
    /// assert_eq!(vec![(None, Some(time)), (Some(time), None)], intervals);
    /// ```
    pub fn intervals(&self) -> IntervalIter {
        IntervalIter {
            iter: self.borders(),
            previous: None,
            exhausted: false,
        }
    }

    /// Splits the timeline at the given time.
    ///
    /// Makes sure the timeline has two intervals separated by the given time. If the timeline is
    /// already split at that time, nothing happens.
    ///
    /// # Parameters
    ///
    /// * `time`: The new border to insert into the timeline (or to ignore, if there's a same one
    ///   already).
    pub fn split(&mut self, time: SystemTime) {
        let mut other = Timeline::new();
        other.borders.push(time);
        self.combine(other);
    }

    /// Combines two timelines together.
    ///
    /// The modified timeline will be split on the borders of both previous timelines. This is
    /// probably best shown in an image:
    ///
    /// ```text
    /// <------|------|---->
    /// <---|--|--|-------->
    ///
    /// <---|--|--|---|---->
    /// ```
    ///
    /// In other words, it is equivalent to splitting one slice by all the borders of the other
    /// (but with less overhead).
    ///
    /// # Parameters
    ///
    /// * `other`: The other timeline to combine with. All the borders from the `other` are
    ///   inserted into the current one.
    pub fn combine(&mut self, other: Timeline) {
        let borders = self.borders
            .drain(..)
            .merge(other.borders.into_iter())
            .dedup()
            .collect();
        self.borders = borders;
    }

    /// Clips the timeline into the given interval.
    ///
    /// The start and end parameters specify the interesting interval of time. Everything to the
    /// left and right to the interval is thrown away and merged into the infinite ends of the
    /// timeline.
    ///
    /// ```text
    /// <---|----|---|--|---->
    ///       ↑      ↑
    /// ======================
    /// <-----|--|---|------->
    /// ```
    ///
    /// # Parameters
    ///
    /// * `start`: The start of the range of interest, or `None` if interested from the beginning
    ///   of all times.
    /// * `end`: Just like `start`, just from the other side.
    pub fn clip(&mut self, start: Option<SystemTime>, end: Option<SystemTime>) {
        if let Some(s) = start {
            self.split(s);
            self.borders.retain(|b| b >= &s);
        }
        if let Some(e) = end {
            self.split(e);
            self.borders.retain(|b| b <= &e);
        }
    }

    /// Returns the index of interval the provided thing falls into.
    ///
    /// It chooses the interval of the timeline the provided slice can be best said it belongs to.
    /// To handle a very finely sliced time line, the slice is approximated by a single point in
    /// the middle and an interval that includes the point is found.
    ///
    /// # Parameters
    ///
    /// * `start`: Start of the interval.
    /// * `end`: End of the interval.
    ///
    /// # Panics
    ///
    /// If the interval is inverted.
    pub fn place(&self, start: SystemTime, end: SystemTime) -> usize {
        let middle = start + end.duration_since(start).expect("Inverted interval") / 2;

        // Find the place this middlepoint is
        match self.borders.binary_search(&middle) {
            // If the middlepoint falls directly onto a boundary, take the interval to the left of
            // it.
            Ok(idx) |
            // If it's not there, we place it to the interval where it would have fallen.
            Err(idx) => idx,
        }
    }

    /// Determines if the timeline is not split.
    ///
    /// An unsplit timeline is one that has only a single interval (-∞, ∞). It is returned by
    /// the [`new`](#method.new).
    pub fn is_unsplit(&self) -> bool {
        self.borders.is_empty()
    }

    /// Constructs a dummy timeline for testing purposes.
    ///
    /// This internally uses the [`from_borders`](#method.from_borders), but it takes the borders
    /// as the number of milliseconds since the epoch. It eases up creation of timelines with known
    /// content without the hassle of manipulating the system times.
    ///
    /// This is available only during tests.
    ///
    /// # Parameters
    ///
    /// * `borders`: A sorted array of unique borders of the intervals, each one as the number of
    ///   milliseconds since the unix epoch.
    ///
    /// # Panics
    ///
    /// The same as in the `from_borders` case.
    #[cfg(test)]
    pub fn dummy(borders: Vec<u64>) -> Self {
        let borders_sys = borders
            .into_iter()
            .map(flow::system_time_from_ms)
            .collect();
        Self::from_borders(borders_sys)
    }
}

impl Serialize for Timeline {
    // We serialize manually, because we want the list of intervals, not borders and we don't store
    // them. We do so by placing each interval into an small serialization helper structure and
    // iterate through these.
    fn serialize<S: Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
        if self.is_unsplit() {
            ser.serialize_none()
        } else {
            #[derive(Serialize)]
            struct IntervalHelper {
                #[serde(skip_serializing_if = "Option::is_none")]
                start: Option<u64>,
                #[serde(skip_serializing_if = "Option::is_none")]
                end: Option<u64>,
            };
            let mut seq = ser.serialize_seq(Some(self.interval_count()))?;
            for (start, end) in self.intervals() {
                let interval = IntervalHelper {
                    start: start.map(flow::system_time_to_ms),
                    end: end.map(flow::system_time_to_ms),
                };
                seq.serialize_element(&interval)?;
            }
            seq.end()
        }
    }
}

/// A helper to deserialize a negative number as duration
fn relative_deserialize<'de, D>(d: D) -> Result<Duration, D::Error>
where
    D: Deserializer<'de>
{
    let ms: i64 = Deserialize::deserialize(d)?;
    assert!(ms < 0,
            "How did a non-negative value get here, when it should have been taken as absolute?");
    Ok(Duration::from_millis(-ms as u64))
}

/// A time specification, as requested by the client.
#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
#[serde(untagged)]
pub enum Time {
    /// An absolute time.
    Absolute(#[serde(deserialize_with = "flow::system_time_deserialize")] SystemTime),
    /// A relative time.
    ///
    /// The relative time is represented as a negative number of milliseconds.
    Relative(#[serde(deserialize_with = "relative_deserialize")] Duration),
}

impl Into<SystemTime> for Time {
    fn into(self) -> SystemTime {
        match self {
            Time::Absolute(abs) => abs,
            Time::Relative(before) => SystemTime::now() - before,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::UNIX_EPOCH;

    use serde_json::{self, Value};

    use super::*;

    /// Tests the creation of the new, virgin timeline
    #[test]
    fn tl_empty() {
        let empty = Timeline::new();
        assert!(empty.is_unsplit());
        empty.assert_sane();
        assert_eq!(0, empty.border_count());
        assert_eq!(1, empty.interval_count());
        let mut borders = empty.borders();
        assert_eq!((0, Some(0)), borders.size_hint());
        assert_eq!(None, borders.next());
        let intervals = empty.intervals();
        assert_eq!((1, Some(1)), intervals.size_hint());
        assert_eq!(vec![(None, None)], intervals.collect::<Vec<_>>());
    }

    /// Tests the invariants and behaviours of from_borders are OK in the sorted scenarios.
    #[test]
    fn tl_from_borders_valid() {
        let empty = Timeline::from_borders(vec![]);
        assert_eq!(Timeline::new(), empty);
        assert!(empty.is_unsplit());
        let a = flow::system_time_from_ms(42);
        let one_border = Timeline::from_borders(vec![a]);
        assert!(!one_border.is_unsplit());
        assert_eq!(1, one_border.border_count());
        assert_eq!(2, one_border.interval_count());
        assert_eq!(vec![&a], one_border.borders().collect::<Vec<_>>());
        assert_eq!(vec![(None, Some(a)), (Some(a), None)],
                   one_border.intervals().collect::<Vec<_>>());
        let b = flow::system_time_from_ms(100);
        let c = flow::system_time_from_ms(5000);
        let many = Timeline::from_borders(vec![a, b, c]);
        assert!(!many.is_unsplit());
        assert_eq!(3, many.border_count());
        assert_eq!(4, many.interval_count());
        assert_eq!(vec![&a, &b, &c], many.borders().collect::<Vec<_>>());
        assert_eq!(vec![
                       (None, Some(a)),
                       (Some(a), Some(b)),
                       (Some(b), Some(c)),
                       (Some(c), None),
                   ], many.intervals().collect::<Vec<_>>());
    }

    /// Tests it refuses to accept wrongly sorted borders
    #[test]
    #[should_panic]
    // The test crashes in musl environments, it seems the stack unwinding is somehow broken. Will
    // be investigated in a separate ticket, as we don't even include the unwind support in the
    // release build.
    #[cfg(not(target_env = "musl"))]
    fn tl_from_borders_inverse() {
        // We use dummy for convenience, but that one uses from_borders internally.
        Timeline::dummy(vec![1, 2, 3, 5, 4]);
    }

    /// Tests it refuses to accept borders with duplica
    #[test]
    #[should_panic]
    // The test crashes in musl environments, it seems the stack unwinding is somehow broken. Will
    // be investigated in a separate ticket, as we don't even include the unwind support in the
    // release build.
    #[cfg(not(target_env = "musl"))]
    fn tl_from_borders_dup() {
        Timeline::dummy(vec![1, 2, 3, 3, 4]);
    }

    /// Test splitting the time lines, both by a single time and by another timeline.
    #[test]
    fn tl_split() {
        let mut tl = Timeline::new();
        assert!(tl.is_unsplit());
        tl.split(flow::system_time_from_ms(42));
        tl.assert_sane();
        assert_eq!(Timeline::dummy(vec![42]), tl);
        assert!(!tl.is_unsplit());
        tl.split(flow::system_time_from_ms(100));
        tl.assert_sane();
        tl.split(flow::system_time_from_ms(50));
        tl.assert_sane();
        let expected_small = Timeline::dummy(vec![42, 50, 100]);
        assert_eq!(expected_small, tl);
        // Splitting once again at the same point has no effect
        tl.split(flow::system_time_from_ms(50));
        tl.assert_sane();
        assert_eq!(expected_small, tl);

        // Splitting by an empty timeline has no effect
        tl.combine(Timeline::new());
        assert!(!tl.is_unsplit());
        tl.assert_sane();
        assert_eq!(expected_small, tl);
        // Splitting it with non-empty splits by all the relevant borders
        let other = Timeline::dummy(vec![25, 42, 80, 120]);
        tl.combine(other.clone());
        tl.assert_sane();
        let expected_big = Timeline::dummy(vec![25, 42, 50, 80, 100, 120]);
        assert_eq!(expected_big, tl);
        // Splitting an empty by an empty produces empty
        tl = Timeline::new();
        tl.combine(Timeline::new());
        assert!(tl.is_unsplit());
        tl.assert_sane();
        assert_eq!(Timeline::new(), tl);
        // Splitting an empty by a non-empty produces the other
        tl.combine(other.clone());
        tl.assert_sane();
        assert_eq!(other, tl);
    }

    /// Tests serialization of the timeline.
    #[test]
    fn tl_serialize() {
        let empty = Timeline::new();
        assert_eq!(Value::Null, serde_json::to_value(&empty).unwrap());

        let with_data = Timeline::dummy(vec![2, 4, 42]);
        let val = json!([
            { "end": 2 },
            { "start": 2, "end": 4 },
            { "start": 4, "end": 42 },
            { "start": 42 }
        ]);
        assert_eq!(val, serde_json::to_value(&with_data).unwrap());
    }

    /// Tests clipping of the interval.
    #[test]
    fn tl_clip() {
        let mut tl = Timeline::dummy(vec![1, 3, 5, 7, 9]);
        let orig = tl.clone();
        tl.clip(None, None);
        tl.assert_sane();
        assert_eq!(orig, tl);
        tl.clip(Some(flow::system_time_from_ms(1)), Some(flow::system_time_from_ms(9)));
        tl.assert_sane();
        assert_eq!(orig, tl);
        tl.clip(Some(flow::system_time_from_ms(0)), Some(flow::system_time_from_ms(10)));
        tl.assert_sane();
        let longer = Timeline::dummy(vec![0, 1, 3, 5, 7, 9, 10]);
        assert_eq!(longer, tl);
        tl.clip(Some(flow::system_time_from_ms(2)), Some(flow::system_time_from_ms(6)));
        tl.assert_sane();
        let shorter = Timeline::dummy(vec![2, 3, 5, 6]);
        assert_eq!(shorter, tl);
    }

    /// Tests finding the correct place inside a timeline.
    #[test]
    fn tl_place() {
        let start = flow::system_time_from_ms(1000);
        let end = flow::system_time_from_ms(2000);
        let empty_tl = Timeline::new();
        assert_eq!(0, empty_tl.place(start, end));
        let split_tl = Timeline::dummy(vec![1000, 2000, 3000]);
        assert_eq!(1, split_tl.place(start, end));
        let later_tl = Timeline::dummy(vec![5000, 6000]);
        assert_eq!(0, later_tl.place(start, end));
        // A very finely sliced timeline
        let fine_tl = Timeline::dummy(vec![1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900]);
        assert_eq!(4, fine_tl.place(start, end));
        // A very finely sliced timeline that does not contain the middlepoint
        let fine_tl = Timeline::dummy(vec![1100, 1200, 1300, 1400, 1600, 1700, 1800, 1900]);
        assert_eq!(4, fine_tl.place(start, end));
    }

    /// Tests that broken interval gets rejected.
    #[test]
    #[should_panic]
    #[cfg(not(target_env = "musl"))]
    fn tl_place_broken() {
        Timeline::new().place(flow::system_time_from_ms(2000), flow::system_time_from_ms(1000));
    }

    /// Tests the deserialization of the time, both absolute and relative.
    #[test]
    fn deser_time() {
        let absolute = serde_json::from_str::<Time>("1000").unwrap();
        assert_eq!(Time::Absolute(flow::system_time_from_ms(1000)), absolute);
        let zero = serde_json::from_str("0").unwrap();
        assert_eq!(Time::Absolute(UNIX_EPOCH), zero);
        let relative = serde_json::from_str("-1000").unwrap();
        assert_eq!(Time::Relative(Duration::from_millis(1000)), relative);
        // Some things that are refused
        serde_json::from_str::<Time>("null").unwrap_err();
        serde_json::from_str::<Time>("{}").unwrap_err();
        serde_json::from_str::<Time>("1.1").unwrap_err();
        serde_json::from_str::<Time>("").unwrap_err();
    }

    /// Tests the conversion of the time to system time.
    #[test]
    fn time_to_system() {
        // The absolute stays the same
        let system = flow::system_time_from_ms(1000);
        let absolute = Time::Absolute(system);
        assert_eq!(system, absolute.into());

        // The relative one is bit harder to test, as the time moves on. But we can estimate it on
        // lower and upper bound (note that we need to take the lower bound before the conversion
        // and the upper bound after).
        let relative = Time::Relative(Duration::from_millis(1000));
        let lower = SystemTime::now() - Duration::from_millis(1000);
        let converted = relative.into();
        let upper = SystemTime::now() - Duration::from_millis(1000);
        assert!(lower <= upper,
                "Time jump when testing time manipulation :-(. Don't be nasty to the test");
        assert!(lower <= converted && converted <= upper);
    }
}
