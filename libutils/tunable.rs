// Copyright 2017, CZ.NIC z.s.p.o. (http://www.nic.cz/)
//
// This file is part of the pakon system.
//
// Pakon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
// Pakon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pakon.  If not, see <http://www.gnu.org/licenses/>.

//! Constants tunable at compile time.

/// A limit in time slices after which we decide the connection is dead.
///
/// As one slice is 5 second long, this is quarter of an hour.
pub const SLICE_INACTIVE_LIMIT: u16 = 15 * 12;

/// Keep a terminated flow for 5 seconds before actually getting rid of it.
pub const SLICE_KEEP_ENDED: u16 = 1;

/// One in-memory slice is 5 seconds long.
pub const SLICE_LENGTH_SECONDS: u64 = 5;

/// Keep one hour of history in RAM.
pub const SLICE_MAX_CACHE: usize = 60 * 12;

/// How long to delay accepting new connections on error.
///
/// If there's a severe error, like too many open files, wait this many milliseconds before trying
/// to accept more connections.
pub const LISTEN_ERR_MILLIS: u64 = 100;

/// How many clients we are willing to handle at once.
pub const MAX_CLIENTS: usize = 100;

/// Maximum number of parallel requests from one client.
pub const MAX_REQUESTS: usize = 10;

/// Size of the updates channel from a data source
pub const DSRC_CHANNEL_SIZE: usize = 1024;
